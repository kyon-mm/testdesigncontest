/**
 * groovy watch.groovyで起動します。
 * 指定時間間隔でファイル変更を検知して指定コマンドを実行します。
 * 終了はプロセスをキルしてください。
 * 同一ディレクトリにあるwatchConfig.groovyに各種設定を行います。
 * path : ファイル変更検知ディレクトリ
 * command : 実行コマンド
 * exclude : 除外対象パスの頭文字
 * interval : 検知間隔時間
 */

def config = new ConfigSlurper().parse(new File("watchConfig.groovy").toURL())
def path = config["path"]
def command = config["command"] as String
def excludeList = config["exclude"] ?: []
def interval = config["interval"] ?: 10 * 1000

println path
println command

def dir = new File(path)
def m = [:]
def isBuildTarget = false

while(true){
    dir.eachFileRecurse{filepath ->
        if(!excludeList.any{filepath.absolutePath.startsWith(dir.absolutePath + "/$it")}){
            if(m[filepath.absolutePath] != filepath.lastModified()){
                println filepath.absolutePath

                isBuildTarget = true
                m << [(filepath.absolutePath): filepath.lastModified()]
            }
        }
    }
    if(isBuildTarget){
        println "Build:${new Date().format("yyyy/MM/dd hh:mm:ss")}"
        def p = command.execute()
        p.waitFor()
        println p.in.text
        println p.err.text
    }
    isBuildTarget = false
    sleep(interval)
}


