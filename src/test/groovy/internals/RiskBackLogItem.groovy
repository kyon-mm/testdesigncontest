package internals

import algorithm.AlgorithmBase
import spock.lang.Specification

class RiskBackLogItem {
    Risk risk
    Actor actor
    List<Class<? extends Specification>> solution
    List target
    int cost = _3.value
    int impact = 0
    Answer functional
    Answer dysFunctional
    def algorithm
    static Cost _1 = new Cost(value: 2)
    static Cost _2 = new Cost(value: 3)
    static Cost _3 = new Cost(value: 4)
    static Cost _5 = new Cost(value: 5)
    static Cost _8 = new Cost(value: 6)
    static Cost _13 = new Cost(value: 7)

    def f = new File(new ConfigSlurper().parse(new File("Config.groovy").toURI().toURL()).riskBackLog_htmlBody)
    RiskBackLogItem isPreventedBy(Class<?>... solution){
        solution.each{
            if(it.superclass == ARRTSpec){
                assert it.newInstance()?.ubiquitous.dictionary() == actor.ubiquitous.dictionary()
            }
            else{
                assert it.classes.every{
                    it.newInstance()?.ubiquitous.dictionary() == actor.ubiquitous.dictionary()
                }
            }
        }
        this.solution = solution
        this
    }
    Class<? extends Specification> isIgnored(){
        f.append("""<div class="c${cost} r${impact}">${BackLogID.instance.value()}. ${risk.name}リスクは無視する.</div>\n""", "utf-8")
        return IgnoreRisk
    }
    public RiskBackLogItem whenResolved(Answer functional){
        this.functional = functional
        this
    }
    public RiskBackLogItem ButWhenNoResolved(Answer dysFunctional){
        this.dysFunctional = dysFunctional
        this.impact = new Impact(functional, dysFunctional).value
        this
    }
    public RiskBackLogItem Cost(Cost i){
        this.cost = i.value
        this
    }
    public <T extends AlgorithmBase> List<Class<? extends Specification>> with(T  algorithm){
        this.algorithm = algorithm
        assert risk.dependsOn?.every{it.isReduced}, "リスクの依存関係を無視しています。先にこれらを回避してください。${risk.dependsOn*.name} "

        risk.isReduced = true
        solution.each{targetSolution ->
            def targetIndex = BackLogID.instance.value()
            def target = getTestTargetNames(targetSolution)
            getSolutionNames(targetSolution).eachWithIndex{solutions, solutionIndex ->
                f.append("""<div class="c${cost} r${impact}">${targetIndex}.${solutionIndex+1}. ${risk.name}リスクを, ${target}の範囲で軽減する. ${solutions}.</div>\n""", "utf-8")
            }
        }
        solution
    }
    static List<String> getSolutionNames(Class targetSolution){
        // TODO assertにする。
        def result = (targetSolution.newInstance() instanceof Specification ?
            ["${targetSolution.newInstance().focusPoint}に着目して${targetSolution.newInstance().specName}"]
            : targetSolution.getClasses()*.newInstance().collect {"${it.focusPoint}に着目して${it.specName}"})
        if(result ? result.any{it.contains("未指定")} : true){
            println "-----"
            println "focusPointかspecNameが未指定です"
            println targetSolution.name
            println result
        }
        result
    }
    static String getTestTargetNames(targetSolution){
        // TODO assertにする。
        def result = targetSolution.newInstance() instanceof Specification ?
            targetSolution.enclosingClass.newInstance().targetName
            : targetSolution.newInstance().targetName
        if(result ? result.contains("未指定") : true){
            println "-----"
            println "TargetNameが未指定です"
            println targetSolution.name
            println result
        }
        result
    }


}
