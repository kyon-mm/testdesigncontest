package internals

import groovy.transform.TupleConstructor

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/17
 * Time: 11:14
 * To change this template use File | Settings | File Templates.
 */
abstract class Stimulation {
    def abstract name()
    def abstract run(args)
    def final execute(args){
        try{
            return new Result(result:run(args))
        }
        catch(e){
            return new Result(ex:e)
        }
    }
    String toString(){
        name()
    }
}
