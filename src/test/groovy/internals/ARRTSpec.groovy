package internals

import actor.CustomerUbiquitous
import algorithm.AlgorithmBase
import groovy.transform.TupleConstructor
import org.codehaus.groovy.runtime.InvokerHelper
import org.spockframework.runtime.model.DataProviderMetadata
import org.spockframework.runtime.model.FeatureMetadata
import org.spockframework.util.ReflectionUtil
import spock.lang.Shared
import spock.lang.Specification
import testdomain.BillSlot

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/15
 * Time: 15:37
 * To change this template use File | Settings | File Templates.
 */
class ARRTSpec extends Specification{
    @Shared
    AlgorithmBase algorithm

    @Shared
    boolean debug = false



    String getSpecName(){
        "未指定"
    }
    String getFocusPoint(){
        "未指定"
    }

    String getActor(){
        ubiquitous.dictionary().tester
    }
    Ubiquitous getUbiquitous(){
        new CustomerUbiquitous()
    }
    Map values() {
        getValidProperties().collectEntries{[it.name, it.getProperty(this)]}
    }

    def setupSpec(){
        def config = new ConfigSlurper().parse(new File("Config.groovy").toURI().toURL())
        def f = new File(config.highLevelTestCase_md)
        f.append("### ${this.specName}\n\n", "utf-8")
    }

    Map randomValues() {
        def ps = getValidProperties()
        def result = ps[new Random().nextInt(ps.size())]
        [(result.name):result.getProperty(this)]
    }

    def test(Parameter preCondition, Stimulation stimulation, Assertion action, Invariant invariant){
        validate(preCondition, stimulation, action)
        export(stimulation, action, preCondition, invariant)
        algorithm.gen(preCondition).each { Parameter parameter ->
            def actual = action.execute(stimulation, parameter)
            def state = invariant.run(actual, parameter)
        }
    }

    def integrate(Class<? extends ARRTSpec> firstBehave, TestCaseFilter firstCondition,Class<? extends ARRTSpec> secondBehave, TestCaseFilter secondCondition){
        def firstCases = cases(firstBehave, firstCondition)
        def secondCases = cases(secondBehave, secondCondition)
        [firstCases, secondCases].combinations().each{TestCaseParameter f, TestCaseParameter s ->
            this.algorithm.gen(f.preCondition).each{Parameter fPrecondition ->
                singleTest(fPrecondition, f.stimulation, f.action, f.invariant)
                this.algorithm.gen(s.preCondition).each{Parameter sPrecondition ->
                    singleTest(sPrecondition, s.stimulation, s.action, s.invariant)
                }
            }
        }
    }

    def singleTest(Parameter preCondition, Stimulation stimulation, Assertion action, Invariant invariant){
        export(stimulation, action, preCondition, invariant)
        def actual = action.execute(stimulation, preCondition)
        def state = invariant.run(actual, preCondition)
    }


    List<TestCaseParameter> cases(Class<? extends ARRTSpec> clazz, TestCaseFilter filter){
        List<Parameter> preCondition = clazz.methods.findAll{
            it.annotations.any{it.annotationType() == DataProviderMetadata} &&
                    (it.annotations[0] as DataProviderMetadata).dataVariables().contains("preCondition")
        }.head().invoke(clazz.newInstance()) as List<Parameter>

        List<Stimulation>  stimulation = clazz.methods.findAll{
            it.annotations.any{it.annotationType() == DataProviderMetadata} &&
                    (it.annotations[0] as DataProviderMetadata).dataVariables().contains("stimulation")
        }.head().invoke(clazz.newInstance()) as List<Stimulation>

        List<Assertion>  action = clazz.methods.findAll{
            it.annotations.any{it.annotationType() == DataProviderMetadata} &&
                    (it.annotations[0] as DataProviderMetadata).dataVariables().contains("action")
        }.head().invoke(clazz.newInstance()) as List<Assertion>

        List<Invariant>  invariant = clazz.methods.findAll{
            it.annotations.any{it.annotationType() == DataProviderMetadata} &&
                    (it.annotations[0] as DataProviderMetadata).dataVariables().contains("invariant")
        }.head().invoke(clazz.newInstance()) as List<Invariant>

        List<Integer> caseNumbers = []
        preCondition.eachWithIndex {Parameter entry, int i->
            if((filter.parameter == TestCaseFilter.Any || entry == filter.parameter)
                    && (filter.stimulation == TestCaseFilter.Any || stimulation[i] == filter.stimulation)){
                caseNumbers += i
            }
        }
        caseNumbers.collect{
            new TestCaseParameter(preCondition[it], stimulation[it], action[it], invariant[it])
        }
    }

    def void export(Stimulation stimulation, Assertion action, Parameter preCondition, Invariant invariant) {
        def config = new ConfigSlurper().parse(new File("Config.groovy").toURI().toURL())
        def f = new File(config.highLevelTestCase_md)
        def text = """|1.  ${this.actor}が${stimulation}と${action}
                |  * ${preCondition._name}
                |  *  -> $stimulation
                |  *  -> $action.
                |  *  -> 常に $invariant.
                |  *  □${preCondition.values().collectEntries { [ubiquitous.dictionary()[(it.key as String).toLowerCase()], it.value] }}\n\n""".stripMargin("|")
        if(debug){
            println text
        }
        f.append(text, "utf-8")
    }

    def void validate(Parameter preCondition, Stimulation stimulation, Assertion action) {
        preCondition.values().each {
            assert ubiquitous.dictionary()[(it.key as String).toLowerCase()] != null, "${it.key} がユビキタス言語にありません。"
        }
        assert stimulation.toString() ==~ /.+[に|を].+る/, "stimulationにバウンダリオブジェクトがないか、最後が動詞(〜る)になっていません。${this.class.name} $stimulation\n"
        assert (action.toString() ==~ /.+[に|を].+る/ || action.toString() == Assertion.DoNothing), "actionにバウンダリオブジェクトがないか、最後が動詞(〜る)になっていません。${this.class.name} $action\n"
    }

    Map values(Closure cls) {
        getValidProperties().findAll{cls.call(it)}.collectEntries{[it.name, it.getProperty(this)]}
    }

    private ArrayList<MetaProperty> getValidProperties() {
        this.metaClass.properties.findAll{ it.name != "class" && it.getProperty(this) != null }
    }

    def invokeMethod(String name, Object args){
        def f = this.specificationContext.iterationInfo?.parent?.parent?.allFeatures?.find {it.name == name}
        def targetMethods = this.class.methods.findAll { it.getAnnotation(FeatureMetadata) }.findAll{it.getAnnotation(FeatureMetadata).name() == name}

        if(f){
            this.algorithm.gen(*args).each{
                this.specificationContext.mockController.enterScope()
                ReflectionUtil.invokeMethod(this, f.getFeatureMethod().getReflection(), it);
            }
        }
        else if(targetMethods){
            def targetMethod = targetMethods.head()
                this.algorithm.gen(*args).each{
                    this.specificationContext.mockController.enterScope()
                    targetMethod.invoke(this, it)
            }
        }
        else{
            println name
            assert false
        }
    }
}
