package internals

import groovy.transform.Canonical
import groovy.transform.TupleConstructor

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/17
 * Time: 11:42
 * To change this template use File | Settings | File Templates.
 */
@TupleConstructor
abstract class Parameter {
    String toString(){
        _name
    }
    Map values() {
        getValidProperties().collectEntries{[it.name, it.getProperty(this)]}
    }
    private ArrayList<MetaProperty> getValidProperties() {
        this.metaClass.properties.findAll{ !["name", "class","_name"].contains(it.name) && it.getProperty(this) != null && it.type != this.class }
    }
}