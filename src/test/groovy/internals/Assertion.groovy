package internals

abstract class Assertion {

    static String DoNothing = "何もしない"

    def abstract name()
    def abstract run(Closure stimulation, Parameter parameter)
    def execute(Stimulation stimulation, Parameter parameter){
        use(ClosureEx){
            run(stimulation.&execute.lazy(parameter), parameter)
        }
   }
    String toString(){
        name()
    }
}
