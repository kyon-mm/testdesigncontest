package internals

/**
 * Created with IntelliJ IDEA.
 * User: kyon_mm
 * Date: 2013/10/29
 * Time: 2:44
 * To change this template use File | Settings | File Templates.
 */
class Impact {
    int value = 0
    Impact(Answer functional, Answer dysFunctional){
        if([Answer.Expect, Answer.Neutral, Answer.LiveWith].contains(functional)
                && Answer.DisLike == dysFunctional){
            this.value = 3
        }
        else if(Answer.Like == functional
                && Answer.DisLike == dysFunctional){
            this.value = 2
        }
        else if(Answer.Like ==  functional
                && [Answer.Expect, Answer.Neutral, Answer.LiveWith].contains(dysFunctional)){
            this.value = 1
        }
        else if(Answer.Like ==  functional
                && Answer.Like == dysFunctional){
            assert "懐疑的回答です。リスクもしくは回答について再検討してください。"
        }
        else if(Answer.DisLike ==  functional
                && Answer.DisLike == dysFunctional){
            assert "懐疑的回答です。リスクもしくは回答について再検討してください。"
        }
        else if([Answer.Expect, Answer.Neutral, Answer.LiveWith, Answer.DisLike].contains(functional)
                && Answer.Like == dysFunctional){
            assert "逆効果です。回答が意味をなしていません。リスクもしくは回答について再検討してください。"
        }
        else if(Answer.DisLike ==  functional
                && [Answer.Like, Answer.Expect, Answer.Neutral, Answer.LiveWith].contains(dysFunctional)){
            assert "逆効果です。回答が意味をなしていません。リスクもしくは回答について再検討してください。"
        }

    }
}
