package internals

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/18
 * Time: 10:15
 * To change this template use File | Settings | File Templates.
 */
class ClosureEx {
    static Closure lazy(Closure self, args) {
        { p -> self.call(args) }
    }
}
