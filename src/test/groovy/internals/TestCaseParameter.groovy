package internals

import groovy.transform.TupleConstructor

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/11/21
 * Time: 17:50
 * To change this template use File | Settings | File Templates.
 */
@TupleConstructor
class TestCaseParameter{
    Parameter preCondition
    Stimulation stimulation
    Assertion action
    Invariant invariant
}

