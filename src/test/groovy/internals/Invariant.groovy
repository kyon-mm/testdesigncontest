package internals

abstract class Invariant {
    def abstract name()
    def abstract run(arg, preCondition)
    String toString(){
        name()
    }

    static Invariant None = new Invariant() {
        @Override
        def name() {
            "不変条件なし"
        }

        @Override
        def run(arg, preCondition) {
            return arg
        }
    }
}
