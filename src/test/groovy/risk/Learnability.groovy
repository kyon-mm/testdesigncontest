package risk

import internals.Risk
import internals.RiskBackLogItem

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/10
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */
class Learnability extends Risk{
    RiskBackLogItem TryInsertMoneyIsHard = new RiskBackLogItem(name: "貨幣を入れる場所がわからない", risk:this)
    RiskBackLogItem TryGetOutMoneyIsHard = new RiskBackLogItem(name: "お金を取る場所がわからない", risk: this)
    RiskBackLogItem KnowRefundIsHard = new RiskBackLogItem(name:"返金されたことに気付かない", risk: this)
}
