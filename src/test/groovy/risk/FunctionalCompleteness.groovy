package risk

import internals.Risk

class FunctionalCompleteness extends Risk {
    static FunctionalCompleteness InsertMoneyIsIgnored = new FunctionalCompleteness(name: "入れた貨幣が無視される")
    static FunctionalCompleteness PushRefundButtonIsIgnored = new FunctionalCompleteness(name: "返金ボタンを押しても無視される")
    static FunctionalCompleteness PushSaleButtonIsIgnored = new FunctionalCompleteness(name: "販売ボタンを押しても無視される")
    static FunctionalCompleteness GetOutJuiceIsRejected = new FunctionalCompleteness(name: "ジュースを取り出せない")
    static FunctionalCompleteness GetOutJuiceIsDifferentFromSuitableTemperature = new FunctionalCompleteness(name: "買ったジュースが表示と異なる温度になっている")
    static FunctionalCompleteness FaultIsIgnored = new FunctionalCompleteness(name: "故障が無視される")
    static FunctionalCompleteness LightLessChangeLampIsIgnored = new FunctionalCompleteness(name: "釣銭切れランプ点灯が無視される")
    static FunctionalCompleteness TimeOutIsIgnore = new FunctionalCompleteness(name: "タイムアウトが無視される")
    static FunctionalCompleteness LightRefundingLampIsIgnore = new FunctionalCompleteness(name: "返金中ランプが無視される")
    static FunctionalCompleteness ConfirmAmountDisplayIsDifferentFromAmount = new FunctionalCompleteness(name: "表示金額と残高が異なる", dependsOn: [InsertMoneyIsIgnored])
    static FunctionalCompleteness DrawIsCanceled = new FunctionalCompleteness(name: "抽選が発生しない", dependsOn: [PushSaleButtonIsIgnored])
    static FunctionalCompleteness BuyProductIsCanceled = new FunctionalCompleteness(name: "商品購入が取り消される")
    static FunctionalCompleteness DrawResultIsIgnored = new FunctionalCompleteness(name: "抽選結果が無視される")
}
