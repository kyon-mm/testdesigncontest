package risk

import internals.Risk
import internals.RiskBackLogItem

/**
 * 操作に関わるリスク
 *  IsCanceled : より複数の操作を持っているとき
 *  IsIgnored : より単体での操作であるとき
 *  IsRejected : どちらでも使えそう
 * モノに関わるリスク
 *  Is形容詞
 */
class TimeBehavior extends Risk{
    RiskBackLogItem WaitInsertMoneyIsLong = new RiskBackLogItem(name:"貨幣投入可能になるまでの時間が長い",  risk: this)
    RiskBackLogItem WaitChangeSaleButtonIsLong = new RiskBackLogItem(name:"購入者の操作に応じた販売ボタンの点灯状態が変更されるまでの時間が長い",  risk: this)
    RiskBackLogItem WaitReceiveJuiceIsLong = new RiskBackLogItem(name:"ジュースが出てくるまでの時間が長い",  risk: this)
    RiskBackLogItem WaitReceiveChangeIsLong = new RiskBackLogItem(name:"つり銭が出てくるまでの時間が長い",  risk: this)
    RiskBackLogItem WaitDrawingIsLong = new RiskBackLogItem(name:"抽選結果が出てくるまでの時間が長い",  risk: this)
}
