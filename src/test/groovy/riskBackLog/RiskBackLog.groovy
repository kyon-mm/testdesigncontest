package riskBackLog

import internals.BackLog
import internals.BackLogs
import org.junit.runner.RunWith
import testdomain.*
import testdomain.explicit.*

import static algorithm.AlgorithmBase.*
import static risk.FunctionalCompleteness.*
import static actor.VendingMachineActor.*
import static internals.RiskBackLogItem.*
import static internals.Answer.*
@RunWith(BackLog)
@BackLogs({->
    def risks = [

            // 次のフォーマットで書いている。これらはIDEの支援により何を選ぶべきか自動補完でサジェストされる。
            // <誰>の<どんなリスク>を<○○>によって軽減する。
            // 解決できたら<うれしい、当然、>だけど、解決できなかったら<>。コストは相対的に<○>ポイントくらい。同値内での組み合わせは<○>くらい。
            // 狩野モデルに関する回答、コストの設定はオプションとなっており、それぞれ優先順位が低い、コストは3ポイント(アジャイルでよく基準にされるポイント)として設定される。
            Customer(InsertMoneyIsIgnored).isPreventedBy(CoinSlot, BillSlot).whenResolved(Expect).ButWhenNoResolved(DisLike).Cost(_8).with(AllCombination),
            Customer(PushRefundButtonIsIgnored).isPreventedBy(RefundButton, CashBox).whenResolved(Expect).ButWhenNoResolved(DisLike).with(AllCombination),
            Customer(PushSaleButtonIsIgnored).isPreventedBy(SaleButton, Rack).whenResolved(Expect).ButWhenNoResolved(DisLike).with(AllCombination),
            Customer(GetOutJuiceIsRejected).isIgnored(), // 物理
            Customer(GetOutJuiceIsDifferentFromSuitableTemperature).isPreventedBy(SaleButton, Rack).whenResolved(Like).ButWhenNoResolved(LiveWith).with(AllCombination),
            Customer(ConfirmAmountDisplayIsDifferentFromAmount).isPreventedBy(CoinSlot, BillSlot, AmountIndicator).whenResolved(Like).ButWhenNoResolved(DisLike).Cost(_13).with(AllCombination),
            Customer(FaultIsIgnored).isPreventedBy(VendingMachineMode, ProductSlotCensor).with(AllCombination),
            Customer(DrawIsCanceled).isPreventedBy(SaleButton, Roulette).whenResolved(Like).ButWhenNoResolved(DisLike).with(AllCombination),
            Customer(DrawResultIsIgnored).isPreventedBy(Roulette).with(AllCombination),
            Customer(LightLessChangeLampIsIgnored).isPreventedBy(LessChangeLamp).with(AllCombination),
            Customer(TimeOutIsIgnore).isPreventedBy(Timeout, SaleButton.DetectPush, CoinSlot, BillSlot).with(AllCombination),
            Customer(LightRefundingLampIsIgnore).isPreventedBy(RefundingLamp).with(AllCombination),
            Customer(BuyProductIsCanceled).isPreventedBy(BuyProduct).with(AllCombination),
    ]
})
class RiskBackLog {
}
