package testdomain

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith

import algorithm.*
import groovy.transform.*
import internals.*
import spock.lang.*

import static algorithm.AlgorithmBase.*
import static internals.Invariant.*
import static spec.RefundButtonSpec.*
import static testdomain.RefundButton.DetectPush.DetectPushParameter.*

@RunWith(Enclosed)
class RefundButton extends Target {

    @Override
    String getTargetName() {
        ubiquitous.dictionary().refundbutton
    }


    static class DetectPush extends ARRTSpec {

        String getFocusPoint() {
            "押す時間"
        }

        String getSpecName() {
            "押されたことを検知する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition      | stimulation | action          | invariant | combination
            detectablePush    | pushButton  | detectPushed    | None      | AllCombination
            notDetectablePush | pushButton  | notDetectPushed | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        @TupleConstructor
        static class DetectPushParameter extends Parameter {
            def _name
            def pushingTime

            static DetectPushParameter detectablePush =
                [pushingTime: DetectableTime, _name: "検知できる時間"]
            static DetectPushParameter notDetectablePush =
                [pushingTime: NotDetectableTime, _name: "検知できない時間"]
        }

        static def pushButton = [
                name: { "ボタンを指定時間分押下する" },
                run: { DetectPushParameter parameter ->
                    given: "検知結果を表示する画面と返金ボタンをつないでおく" //たとえば画面とつないでテストできるとしたら
                    and: "画面に何も表示されていない"
                }
        ] as Stimulation

        static def detectPushed = [
                name: { "画面に返金と表示される" },
                run: { stimulation, DetectPushParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に返金と表示されている"
                    result
                }
        ] as Assertion

        static def notDetectPushed = [
                name: { Assertion.DoNothing },
                run: { stimulation, DetectPushParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "何も起こらない"
                    result
                }
        ] as Assertion
    }
}
