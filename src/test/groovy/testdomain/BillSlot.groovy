package testdomain

import algorithm.AlgorithmBase
import groovy.transform.TupleConstructor
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*
import spec.BillSlotSpec
import spock.lang.Unroll

import static internals.Invariant.*
import static spec.BillSlotSpec.*

import static testdomain.BillSlot.BillType.BillTypeParameter.*
import static testdomain.BillSlot.NumberOfBill.NumberOfBillParameter.*

@RunWith(Enclosed)
class BillSlot extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().billslot
    }

    static class BillType extends ARRTSpec {
        String getSpecName() {
            "紙幣を投入する"
        }

        @Override
        String getFocusPoint() {
            "紙幣の種類"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition | stimulation | action | invariant | combination
            AcceptBill   | InsertBill  | Accept | None      | AlgorithmBase.AllCombination
            ErrorBill    | InsertBill  | Refund | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class BillTypeParameter extends Parameter {
            def _name
            def bill

            static BillTypeParameter AcceptBill =
                [bill: NormalBillTypes, _name: "受け付けるモードで対象日本紙幣"]
            static BillTypeParameter ErrorBill =
                [bill: ErrorBillTypes, _name: "全てのモードで非対象日本紙幣"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def InsertBill = [
                name: { "紙幣を入れる" },
                run: { Parameter parameter ->
                    given: "検知結果を表示する画面と紙幣投入口をつないでおく" //たとえば画面とつないでテストできるとしたら
                    and: "画面に何も表示されていない"
                    if (parameter.diffWithInsertLimit) {
                        and: "#parameter.bill を 上限値 - #parameter.diffWithInsertLimit 枚まで入れる"
                        BillSlotSpec.insertLimit(parameter.bill) - parameter.diffWithInsertLimit
                    } else {
                        and: "投入済み貨幣がない"
                    }
                    when: "#parameter.bill を入れる"
                }
        ] as Stimulation

        static def Accept = [
                name: { "画面に紙幣の種類が表示される" },
                run: { stimulation, preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に" + preCondition.bill + "と表示されている"
                    result
                }
        ] as Assertion

        static def Refund = [
                name: { "画面に返金と表示される" },
                run: { stimulation, preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に返金と表示されている"
                    and: "紙幣投入口から投入した紙幣が排出されている"
                    result
                }
        ] as Assertion
    }

    static class NumberOfBill extends ARRTSpec {
        String getSpecName() {
            "紙幣を投入する"
        }

        @Override
        String getFocusPoint() {
            "投入済み紙幣枚数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition              | stimulation         | action          | invariant | combination
            LessThanInsertLimitNumber | BillType.InsertBill | BillType.Accept | None      | AlgorithmBase.AllCombination
            MoreThanInsertLimitNumber | BillType.InsertBill | BillType.Refund | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class NumberOfBillParameter extends Parameter {
            def _name
            def bill
            def diffWithInsertLimit

            static NumberOfBillParameter LessThanInsertLimitNumber =
                [bill: NormalBillTypes, diffWithInsertLimit: 0, _name: "投入済枚数が上限より少ない"]
            static NumberOfBillParameter MoreThanInsertLimitNumber =
                [bill: NormalBillTypes, diffWithInsertLimit: -1, _name: "投入済枚数が上限以上"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination
    }
}
