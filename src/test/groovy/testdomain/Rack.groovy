package testdomain

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith

import algorithm.*
import groovy.transform.*
import internals.*
import spock.lang.*

import static algorithm.AlgorithmBase.*
import static internals.Invariant.*
import static testdomain.Rack.SendProduct.SendProductParameter.*

import static spec.RackSpec.*

@RunWith(Enclosed)
class Rack extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().rack
    }

    static class NotifyEmpty extends ARRTSpec {
        String getFocusPoint() {
            "ラック内の商品数"
        }

        String getSpecName() {
            "商品がなくなったことを通知する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition | stimulation | action      | invariant | combination
            HaveStock    | empty       | notifyEmpty | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        static def empty = [
                name: { "ラック内を空にする" },
                run: { SendProduct.SendProductParameter parameter ->
                    given: "商品を" + parameter.stockNumber + "本入れておく"
                }
        ] as Stimulation

        static def notifyEmpty = [
                name: { "画面に売り切れと表示する" },
                run: { stimulation, SendProduct.SendProductParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に売り切れ" + preCondition.rack + "と表示されている"
                    result
                }
        ] as Assertion

    }

    static class SendProduct extends ARRTSpec {
        String getFocusPoint() {
            "ラック内の商品数"
        }

        String getSpecName() {
            "商品を送出する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition | stimulation       | action      | invariant | combination
            HaveStock    | notifySendProduct | sendProduct | None      | AllCombination
            NoStock      | notifySendProduct | doNothing   | None      | AllCombination

        }

        @TupleConstructor
        static class SendProductParameter extends Parameter {
            def _name
            def rack
            def stockNumber

            static SendProductParameter HaveStock =
                [rack: AllRack, stockNumber: ExistStockNumber, _name: "ラック内に商品がある"]
            static SendProductParameter NoStock =
                [rack: AllRack, stockNumber: NoStockNumber, _name: "ラック内に商品がない"]
        }

        AlgorithmBase algorithm = AllCombination

        static def notifySendProduct = [
                name: { "商品送出を通知する" },
                run: { SendProductParameter parameter ->
                    given: "ラック内の商品数を" + parameter.stockNumber + "にしておく"
                }
        ] as Stimulation

        static def sendProduct = [
                name: { "商品を送出する" },
                run: { stimulation, SendProductParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "商品が送出されている"
                    result
                }
        ] as Assertion

        static def doNothing = [
                name: { Assertion.DoNothing },
                run: { stimulation, SendProductParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "何も起こらない"
                    result
                }
        ] as Assertion
    }
}
