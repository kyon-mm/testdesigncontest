package testdomain

import algorithm.AlgorithmBase
import static internals.Invariant.*
import groovy.transform.TupleConstructor
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*
import spock.lang.Unroll

import static spec.AmountIndicatorSpec.*

import static testdomain.AmountIndicator.DisplayAmount.DisplayAmountParameter.*

@RunWith(Enclosed)
class AmountIndicator extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().amountindicator
    }

    static class DisplayAmount extends ARRTSpec {
        String getSpecName() {
            "通知された残高を表示する"
        }

        @Override
        String getFocusPoint() {
            "表示する残高の桁数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition   | stimulation  | action  | invariant | combination
            _1ColumnAmount | NotifyAmount | Display | None      | AlgorithmBase.AllCombination
            _2ColumnAmount | NotifyAmount | Display | None      | AlgorithmBase.AllCombination
            _3ColumnAmount | NotifyAmount | Display | None      | AlgorithmBase.AllCombination
            _4ColumnAmount | NotifyAmount | Display | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class DisplayAmountParameter extends Parameter {
            def _name
            def amount
            def displayedAmount

            static DisplayAmountParameter _1ColumnAmount =
                [amount: _1Column, displayedAmount: AllColumnRandom, _name: "1桁の残高"]
            static DisplayAmountParameter _2ColumnAmount =
                [amount: _2Column, displayedAmount: AllColumnRandom, _name: "2桁の残高"]
            static DisplayAmountParameter _3ColumnAmount =
                [amount: _3Column, displayedAmount: AllColumnRandom, _name: "3桁の残高"]
            static DisplayAmountParameter _4ColumnAmount =
                [amount: _4Column, displayedAmount: AllColumnRandom, _name: "4桁の残高"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def NotifyAmount = [
                name: { "残高を通知する" },
                run: { DisplayAmountParameter parameter ->
                    given: "金額表示機を通知する機械とつないでおく"
                    and: parameter.displayedAmount + "を表示させておく"
                    when: "#parameter.amount を通知する"
                }
        ] as Stimulation

        static def Display = [
                name: { "金額表示機に残高を表示する" },
                run: { stimulation, DisplayAmountParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "金額表示機に" + preCondition.amount + "と表示されている"
                    result
                }
        ] as Assertion
    }
}
