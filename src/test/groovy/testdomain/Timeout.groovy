package testdomain

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*

@RunWith(Enclosed)
class Timeout extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().timeout
    }


    static class SendTimeoutSignal extends ARRTSpec {
        String getFocusPoint() {
            "貨幣投入後経過時間"
        }

        String getSpecName(){
            "タイムアウトを検知して通知する"
        }
    }
}