package testdomain

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith

import algorithm.*
import groovy.transform.*
import internals.*
import spock.lang.*

import static algorithm.AlgorithmBase.*
import static internals.Invariant.*
import static testdomain.SaleButton.TurnOffAllLampTest.TurnOffParameter.*
import static testdomain.SaleButton.DetectPush.DetectPushParameter.*

import static spec.SaleButtonSpec.*
import static spec.SaleButtonSpec.SaleButtonState.*

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/10
 * Time: 14:29
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Enclosed)
class SaleButton extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().salebutton
    }

    static class TurnOffAllLampTest extends ARRTSpec {
        String getFocusPoint() {
            "販売ボタンの状態"
        }

        String getSpecName() {
            "すべてのランプを消灯する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation   | action         | invariant | combination
            AllSaleButtonState | NotifyTurnOff | TurnOffAllLamp | None      | AllCombination
        }

        @TupleConstructor
        static class TurnOffParameter extends Parameter {
            def _name
            def saleButtonState

            static TurnOffParameter AllSaleButtonState =
                [saleButtonState: AllState, _name: "すべての販売ボタンの状態"]
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyTurnOff = [
                name: { "消灯通知をする" },
                run: { TurnOffParameter parameter ->
                    given: "通知する機器と販売ボタンをつないでおく"
                }
        ] as Stimulation


        static def TurnOffAllLamp = [
                name: { "すべてのランプを消灯する" },
                run: { stimulation, TurnOffParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "すべてのランプが消灯している"
                    result
                }
        ] as Assertion
    }

    static class LightSaleButtonTest extends ARRTSpec {
        String getFocusPoint() {
            "販売ボタンの状態"
        }

        String getSpecName() {
            "販売ボタンを点灯する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation           | action          | invariant | combination
            AllSaleButtonState | NotifyLightSaleButton | LightSaleButton | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyLightSaleButton = [
                name: { "販売ボタン点灯通知をする" },
                run: { TurnOffAllLampTest.TurnOffParameter parameter ->
                    given: "販売ボタンを parameter.saleButtonState の状態にしておく"
                }
        ] as Stimulation

        static def LightSaleButton = [
                name: { "販売ボタンを点灯する" },
                run: { stimulation, TurnOffAllLampTest.TurnOffParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "販売ボタンが点灯している"
                    result
                }
        ] as Assertion
    }

    static class FlashSaleButtonTest extends ARRTSpec {
        String getFocusPoint() {
            "販売ボタンの状態"
        }

        String getSpecName() {
            "販売ボタンを点滅する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation           | action          | invariant | combination
            AllSaleButtonState | NotifyFlashSaleButton | FlashSaleButton | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyFlashSaleButton = [
                name: { "販売ボタン点滅通知をする" },
                run: { TurnOffAllLampTest.TurnOffParameter parameter ->
                    given: "販売ボタンを parameter.saleButtonState の状態にしておく"
                }
        ] as Stimulation

        static def FlashSaleButton = [
                name: { "販売ボタンを点滅する" },
                run: { stimulation, TurnOffAllLampTest.TurnOffParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "販売ボタンが点滅している"
                    result
                }
        ] as Assertion
    }

    static class LightPreparingLampTest extends ARRTSpec {
        String getFocusPoint() {
            "販売ボタンの状態"
        }

        String getSpecName() {
            "準備中ランプを点灯する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation              | action             | invariant | combination
            AllSaleButtonState | NotifyLightPreparingLamp | LightPreparingLamp | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyLightPreparingLamp = [
                name: { "準備中ランプ点灯通知をする" },
                run: { TurnOffAllLampTest.TurnOffParameter parameter ->
                    given: "販売ボタンを parameter.saleButtonState の状態にしておく"
                }
        ] as Stimulation

        static def LightPreparingLamp = [
                name: { "準備中ランプを点灯する" },
                run: { stimulation, TurnOffAllLampTest.TurnOffParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "準備中ランプが点灯している"
                    result
                }
        ] as Assertion
    }

    static class LightSoldoutLampTest extends ARRTSpec {
        String getFocusPoint() {
            "販売ボタンの状態"
        }

        String getSpecName() {
            "売切表示ランプを点灯する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation            | action           | invariant | combination
            AllSaleButtonState | NotifyLightSoldoutLamp | LightSoldoutLamp | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyLightSoldoutLamp = [
                name: { "売切表示ランプ点灯通知をする" },
                run: { TurnOffAllLampTest.TurnOffParameter parameter ->
                    given: "販売ボタンを parameter.saleButtonState の状態にしておく"
                }
        ] as Stimulation

        static def LightSoldoutLamp = [
                name: { "売切表示ランプを点灯する" },
                run: { stimulation, TurnOffAllLampTest.TurnOffParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "売切表示ランプが点灯している"
                    result
                }
        ] as Assertion
    }

    static class DetectPush extends ARRTSpec {
        String getFocusPoint() {
            "押下する時間"
        }

        String getSpecName() {
            "押されたことを検知する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition                       | stimulation | action          | invariant | combination
            DetectablePush                     | PushButton  | DetectPushed    | None      | AllCombination
            NotDetectablePushBySaleButtonState | PushButton  | NotDetectPushed | None      | AllCombination
            NotDetectablePushByPushingTime     | PushButton  | NotDetectPushed | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        @TupleConstructor
        static class DetectPushParameter extends Parameter {
            def _name
            def saleButton
            def saleButtonState
            def pushingTime

            static DetectPushParameter DetectablePush =
                [saleButton: AllButton, saleButtonState: PushableState, pushingTime: DetectableTime, _name: "押せる状態のボタンと検知できる時間"]
            static DetectPushParameter NotDetectablePushBySaleButtonState =
                [saleButton: AllButton, saleButtonState: NotPushableState, pushingTime: DetectableTime, _name: "押せない状態のボタンと検知できる時間"]
            static DetectPushParameter NotDetectablePushByPushingTime =
                [saleButton: AllButton, saleButtonState: PushableState, pushingTime: NotDetectableTime, _name: "押せる状態のボタンと検知できない時間"]
        }

        static def PushButton = [
                name: { "ボタンを指定時間分押下する" },
                run: { DetectPushParameter parameter ->
                    given: "検知結果を表示する画面と返金ボタンをつないでおく" //たとえば画面とつないでテストできるとしたら
                    and: "画面に何も表示されていない"
                }
        ] as Stimulation

        static def DetectPushed = [
                name: { "画面に販売ボタン番号が表示される" },
                run: { stimulation, DetectPushParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に" + preCondition.saleButton + "と表示されている"
                    result
                }
        ] as Assertion

        static def NotDetectPushed = [
                name: { Assertion.DoNothing },
                run: { stimulation, DetectPushParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "何も起こらない"
                    result
                }
        ] as Assertion
    }
}
