package testdomain.explicit

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/10
 * Time: 14:30
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Enclosed)
class VendingMachineMode extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().mode
    }

    static class ShiftToNotSaleTime extends ARRTSpec {
        String getFocusPoint() {
            "商品送出時間"
        }

        String getSpecName(){
            "販売停止モードに切り替える"
        }
//        @Unroll
//        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
//            expect:
//            test(preCondition, stimulation, action, invariant)
//            where:
//            preCondition              | stimulation | action          | invariant                | combination
//            SAMPLE | sampleStimulation | sampleAction | sampleInvariant | AllCombination
//        }
//
//        @TupleConstructor
//        static class ShiftToNotSaleTimeParameter extends Parameter {
//            def _name
//            def factor1
//
//            static ShiftToNotSaleTimeParameter SAMPLE =
//                [factor1: [1,2,3], _name: "AAA"]
//        }
//
//        AlgorithmBase algorithm = AllCombination
//
//        static def sampleStimulation = [
//                stateName: { "テスト対象への操作、刺激" },
//                run: { ShiftToNotSaleTimeParameter parameter ->
//                    given: "事前準備をする"
//                }
//        ] as Stimulation
//
//        static def sampleAction = [
//                stateName: { "事後条件" },
//                run: { stimulation, ShiftToNotSaleTimeParameter preCondition ->
//                    when:
//                    def result = stimulation()
//                    then: "○○が〜〜でになっている"
//                    result
//                }
//        ] as Assertion
//
//        static def sampleInvariant = [
//                stateName: { "不変条件" },
//                run: { state ->
//                    assert state != null
//                }
//        ] as Invariant

    }
}
