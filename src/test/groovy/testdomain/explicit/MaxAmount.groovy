package testdomain.explicit

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import groovy.transform.*
import spock.lang.*
import algorithm.*
import internals.*

import static algorithm.AlgorithmBase.*
import static internals.Invariant.*

import static spec.implicit.ImplicitMaxAmountSpec.*

import static testdomain.explicit.MaxAmount.Amount.AmountParameter.*

@RunWith(Enclosed)
class MaxAmount extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().maxamount
    }

    static class Amount extends ARRTSpec {
        String getSpecName() {
            "投入可能最大残高を問い合わせる"
        }

        @Override
        String getFocusPoint() {
            "設定した投入可能最大残高"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation   | action         | invariant | combination
            SpecifiedMaxAmount | NotifyInquiry | NotifiedAmount | None      | AllCombination
        }

        @TupleConstructor
        static class AmountParameter extends Parameter {
            def _name
            def amount

            static AmountParameter SpecifiedMaxAmount =
                [amount: MaxAmounts, _name: "指定した投入可能最大残高"]
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyInquiry = [
                name: { "問い合わせ通知をする" },
                run: { AmountParameter parameter ->
                    given: "マスターCPUを通知する機械とつないでおく"
                    and: "マスターCPUを通知された結果を表示する画面とつないでおく"
                    and: "画面に何も表示されていない"
                    and: parameter.amount + "を一度に投入可能な最大金額に設定しておく"
                    when: "問い合わせ通知をする"
                }
        ] as Stimulation

        static def NotifiedAmount = [
                name: { "画面に設定された残高が表示される" },
                run: { stimulation, AmountParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に設定された残高が表示されている"
                    result
                }
        ] as Assertion
    }
}