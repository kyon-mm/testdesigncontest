package testdomain

import actor.CustomerUbiquitous
import algorithm.AlgorithmBase
import groovy.transform.TupleConstructor
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*
import spec.Coin
import spock.lang.Unroll

import static internals.Invariant.*
import static spec.Coin.*
import static spec.CoinSlotSpec.*
import static spec.CoinSlotSpec.InsertedCoinNumber.*

import static testdomain.CoinSlot.CoinType.CoinTypeParameter.*
import static testdomain.CoinSlot.NumberOfCoin.NumberOfCoinParameter.*

@RunWith(Enclosed)
class CoinSlot extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().coinslot
    }

    static class CoinType extends ARRTSpec {
        String getSpecName() {
            "硬貨を投入する"
        }

        @Override
        String getFocusPoint() {
            "硬貨の種類"
        }

        Ubiquitous getUbiquitous() {
            new CustomerUbiquitous()
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition | stimulation | action | invariant | combination
            NormalCoin   | InsertCoin  | Accept | None      | AlgorithmBase.AllCombination
            ErrorCoin    | InsertCoin  | Refund | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class CoinTypeParameter extends Parameter {
            def _name
            def coin

            static CoinTypeParameter NormalCoin =
                [coin: NormalCoinTypes, _name: "対象日本硬貨"]
            static CoinTypeParameter ErrorCoin =
                [coin: ErrorCoinTypes, _name: "非対象硬貨"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def InsertCoin = [
                name: { "硬貨を1枚いれる" },
                run: { Parameter parameter ->
                    given: "検知結果を表示する画面と硬貨投入口をつないでおく" //たとえば画面とつないでテストできるとしたら
                    and: "画面に何も表示されていない"

                    if (parameter.diffWithInsertLimit) {
                        and: "#parameter.coin を投入上限枚数 - #parameter.diffWithInsertLimit 枚まで入れておく"
                        insertLimit(parameter.coin) - parameter.diffWithInsertLimit //指定枚数
                    } else if (parameter.insertedCoinNumber) {
                        and: "投入済硬貨を #insertedCoinNumber にしておく"
                    } else {
                        and: "投入済み貨幣がない"
                    }
                    when: "硬貨を1枚入れる"

                }
        ] as Stimulation

        static def Accept = [
                name: { "画面に硬貨の種類が表示される" },
                run: { stimulation, preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に" + preCondition.coin + "と表示されている"
                    result
                }
        ] as Assertion

        static def Refund = [
                name: { "画面に返金と表示される" },
                run: { stimulation, preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に返金と表示されている"
                    result
                }
        ] as Assertion
    }

    static class NumberOfCoin extends ARRTSpec {
        String getSpecName() {
            "硬貨を投入する"
        }

        @Override
        String getFocusPoint() {
            "同硬貨の投入済み枚数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition              | stimulation         | action          | invariant | combination
            LessThanInsertLimitNumber | CoinType.InsertCoin | CoinType.Accept | None      | AlgorithmBase.AllCombination
            MoreThanInsertLimitNumber | CoinType.InsertCoin | CoinType.Refund | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class NumberOfCoinParameter extends Parameter {
            def _name
            def coin
            def diffWithInsertLimit

            static NumberOfCoinParameter LessThanInsertLimitNumber =
                [coin: NormalCoinTypes, diffWithInsertLimit: -1, _name: "投入済枚数が上限より少ない"]
            static NumberOfCoinParameter MoreThanInsertLimitNumber =
                [coin: NormalCoinTypes, diffWithInsertLimit: 0, _name: "投入済枚数が上限以上"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination
    }

    static class NumberOfCoins extends ARRTSpec {
        String getSpecName() {
            "硬貨を投入する"
        }

        @Override
        String getFocusPoint() {
            "他硬貨の投入済み枚数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition                                 | stimulation         | action          | invariant | combination
            NumberOfCoinsParameter.InsertableAllCoin     | CoinType.InsertCoin | CoinType.Accept | None      | AlgorithmBase.AllCombination
            NumberOfCoinsParameter.MaxAllCoin            | CoinType.InsertCoin | CoinType.Refund | None      | AlgorithmBase.AllCombination
            NumberOfCoinsParameter.MaxAllCoinExclude_10  | CoinType.InsertCoin | CoinType.Accept | None      | AlgorithmBase.AllCombination
            NumberOfCoinsParameter.MaxAllCoinExclude_50  | CoinType.InsertCoin | CoinType.Accept | None      | AlgorithmBase.AllCombination
            NumberOfCoinsParameter.MaxAllCoinExclude_100 | CoinType.InsertCoin | CoinType.Accept | None      | AlgorithmBase.AllCombination
            NumberOfCoinsParameter.MaxAllCoinExclude_500 | CoinType.InsertCoin | CoinType.Accept | None      | AlgorithmBase.AllCombination
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        @TupleConstructor
        static class NumberOfCoinsParameter extends Parameter {
            def _name
            def coin
            def insertedCoinNumber

            static NumberOfCoinsParameter InsertableAllCoin =
                [coin: NormalCoinTypes, insertedCoinNumber: InsertableAll, _name: "どの硬貨もあと1枚投入可能"]
            static NumberOfCoinsParameter MaxAllCoin =
                [coin: NormalCoinTypes, insertedCoinNumber: MaxAll, _name: "どの硬貨も投入不可能"]
            static NumberOfCoinsParameter MaxAllCoinExclude_10 =
                [coin: _10, insertedCoinNumber: MaxExclude_10, _name: "10円硬貨のみ1枚投入可能"]
            static NumberOfCoinsParameter MaxAllCoinExclude_50 =
                [coin: _50, insertedCoinNumber: MaxExclude_50, _name: "50円硬貨のみ1枚投入可能"]
            static NumberOfCoinsParameter MaxAllCoinExclude_100 =
                [coin: _100, insertedCoinNumber: MaxExclude_100, _name: "100円硬貨のみ1枚投入可能"]
            static NumberOfCoinsParameter MaxAllCoinExclude_500 =
                [coin: _500, insertedCoinNumber: MaxExclude_500, _name: "500円硬貨のみ1枚投入可能"]
        }
    }
}
