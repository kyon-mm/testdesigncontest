package testdomain

import algorithm.AlgorithmBase
import groovy.transform.TupleConstructor
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*
import spock.lang.Unroll

import static internals.Invariant.*

import static spec.RefundingLampSpec.*

import static testdomain.RefundingLamp.FlashRefundingLamp.FlashRefundingLampParameter.*

@RunWith(Enclosed)
class RefundingLamp extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().refundinglamp
    }

    static class FlashRefundingLamp extends ARRTSpec {

        String getFocusPoint() {
            "釣銭払出動作中表示ランプの状態"
        }

        String getSpecName() {
            "点滅する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation | action | invariant | combination
            RefundingLampState | NotifyFlash | Flash  | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class FlashRefundingLampParameter extends Parameter {
            def _name
            def lampState

            static FlashRefundingLampParameter RefundingLampState =
                [lampState: [Flashing, NotFlashing], _name: "ランプの点滅状態"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def NotifyFlash = [
                name: { "点滅通知をする" },
                run: { FlashRefundingLampParameter parameter ->
                    given: "通知する機器と釣銭払出動作中表示ランプをつないでおく"
                }
        ] as Stimulation

        static def Flash = [
                name: { "釣銭払出動作中表示ランプを点滅する" },
                run: { stimulation, FlashRefundingLampParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "釣銭払出動作中表示ランプが点滅している"
                    result
                }
        ] as Assertion
    }

    static class TurnOffRefundingLamp extends ARRTSpec {
        String getSpecName() {
            "消灯する"
        }

        @Override
        String getFocusPoint() {
            "釣銭払出動作中表示ランプの状態"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition       | stimulation   | action  | invariant | combination
            RefundingLampState | NotifyTurnOff | TurnOff | None      | AlgorithmBase.AllCombination
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def NotifyTurnOff = [
                name: { "消灯通知をする" },
                run: { Parameter parameter ->
                    given: "通知する機器と釣銭払出動作中表示ランプをつないでおく"
                }
        ] as Stimulation

        static def TurnOff = [
                name: { "釣銭払出動作中表示ランプを消灯する" },
                run: { stimulation, Parameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "釣銭払出動作中表示ランプが消灯している"
                    result
                }
        ] as Assertion
    }
}