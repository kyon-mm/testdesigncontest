package testdomain

import algorithm.AlgorithmBase
import groovy.transform.TupleConstructor
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*
import spock.lang.Unroll

import static algorithm.AlgorithmBase.*
import static internals.Invariant.*

import static spec.implicit.ImplicitRoulette.*

import static testdomain.Roulette.StartRoulette.StartRouletteParameter.*
import static testdomain.Roulette.DisplayDrawResult.DisplayDrawResultParameter.*
import static testdomain.Roulette.WinWithFixedRate.WinWithFixedRateParameter.*

@RunWith(Enclosed)
class Roulette extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().roulette
    }

    static class StartRoulette extends ARRTSpec {

        String getSpecName() {
            "抽選を実施する"
        }

        String getFocusPoint() {
            "抽選時間"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition      | stimulation | action                    | invariant | combination
            SpecifiedTurnTime | NotifyDraw  | StartAndStopAfterTurnTime | None      | AllCombination
        }

        @TupleConstructor
        static class StartRouletteParameter extends Parameter {
            def _name
            def turnTime

            static StartRouletteParameter SpecifiedTurnTime =
                [turnTime: TurnTimes, _name: "指定した抽選時間"]
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyDraw = [
                name: { "抽選通知をする" },
                run: { StartRouletteParameter parameter ->
                    given: "通知する機器と懸賞ルーレットをつないでおく"
                    and: "結果を表示する画面と懸賞ルーレットをつないでおく" //たとえば画面とつないでテストできるとしたら
                    and: "画面に何も表示されていない"
                    when: "抽選通知をする"
                }
        ] as Stimulation

        static def StartAndStopAfterTurnTime = [
                name: { "懸賞ルーレットが指定時間回った後で止まって結果を表示する" },
                run: { stimulation, StartRouletteParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "懸賞ルーレットの周りにあるランプの1つが点灯する"
                    and: "#preCondition.turnTime 秒間、他のランプが右回りの順番に点灯・消灯し、点灯するたびに「ピピッ」という音がする"
                    and: "初めに点灯したランプで止まった場合、「あたり」ランプが点灯するとともに、ファンファーレ音が鳴り、画面に当たりと表示される"
                    and: "初めに点灯したランプ以外で止まった場合、「残念！はずれ」ランプが点灯するとともに、音が止まり、画面にはずれと表示される"
                    and: "画面に抽選結果が表示されている"
                    and: "どちらのランプの場合も5秒後消灯する"
                }
        ] as Assertion
    }

    static class DisplayDrawResult extends ARRTSpec {
        String getSpecName() {
            "抽選結果を提示する"
        }

        @Override
        String getFocusPoint() {
            "当たる確率"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition      | stimulation              | action      | invariant | combination
            AlwaysLoseRateSet | StartRoulette.NotifyDraw | DisplayLose | None      | AllCombination
            AlwaysWinRateSet  | StartRoulette.NotifyDraw | DisplayWin  | None      | AllCombination
        }

        AlgorithmBase algorithm = AllCombination

        @TupleConstructor
        static class DisplayDrawResultParameter extends Parameter {
            def _name
            def winRate

            static DisplayDrawResultParameter AlwaysLoseRateSet =
                [winRate: AlwaysLose, _name: "必ずはずれる設定"]
            static DisplayDrawResultParameter AlwaysWinRateSet =
                [winRate: AlwaysWin, _name: "必ず当たる設定"]
        }

        static def DisplayWin = [
                name: { "当たったことを提示する" },
                run: { stimulation, Parameter parameter ->
                    when:
                    def result = stimulation()
                    then: "「当たり」ランプを点灯する"
                    and: "ファンファーレ音が鳴る"
                    and: "画面に当たりと表示される"
                    and: "5秒後「当たり」ランプを消灯する"
                    result
                }
        ] as Assertion

        static def DisplayLose = [
                name: { "はずれたことを提示する" },
                run: { stimulation, Parameter parameter ->
                    when:
                    def result = stimulation()
                    then: "「残念！はずれ」ランプを点灯する"
                    and: "画面にはずれと表示される"
                    and: "5秒後「残念！はずれ」ランプを消灯する"
                    result
                }
        ] as Assertion
    }

    static class WinWithFixedRate extends ARRTSpec {
        String getSpecName() {
            "抽選したら一定の確率で当たる"
        }

        @Override
        String getFocusPoint() {
            "当たる確率と抽選回数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition | stimulation      | action               | invariant | combination
            Times        | NotifyDrawNTimes | WinWithSpecifiedRate | None      | AllCombination
        }

        @TupleConstructor
        static class WinWithFixedRateParameter extends Parameter {
            def _name
            def winRate
            def time

            static WinWithFixedRateParameter Times =
                [winRate: WinRates, time: [100, 1000, 100000, Integer.MAX_VALUE], _name: "抽選回数"]
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyDrawNTimes = [
                name: { "指定回数分抽選通知をする" },
                run: { WinWithFixedRateParameter parameter ->
                    given: "通知する機器と懸賞ルーレットをつないでおく"
                    and: "結果を表示する画面と懸賞ルーレットをつないでおく" //たとえば画面とつないでテストできるとしたら
                    and: "当たりとはずれの回数をそれぞれ数える機器とつないでおく"
                    and: "画面に何も表示されていない"
                    when: "#parameter.time 回、抽選通知をする"
                }
        ] as Stimulation

        static def WinWithSpecifiedRate = [
                name: { "指定した確率通りの当たり回数を表示する" },
                run: { stimulation, WinWithFixedRateParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "抽選通知をした回数に対して指定した確率通りの当たり回数が表示されている"
//                    def expectWinRate = preCondition.winRate *  0.01
//                    def actual = result / preCondition.time
//                    assert (expectWinRate - actual).abs() < ExplicitRoulette.ErrorRate * 0.01
                }
        ] as Assertion
    }
}