package testdomain

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith

import algorithm.*
import groovy.transform.*
import internals.*
import spec.CashBoxSpec
import spock.lang.*

import static algorithm.AlgorithmBase.*

import static internals.Invariant.*

import static testdomain.CashBox.SendBill.SendBillParameter.*
import static testdomain.CashBox.SendLeastNumberOfCoin.SendLeastNumberOfCoinParameter.*
import static testdomain.CashBox.SendLessNumberOfCoinAboutLessNumber.SendLessNumberOfCoinAboutLessNumberParameter.*
import static testdomain.CashBox.SendLessNumberOfCoinAboutStoreNumber.SendLessNumberOfCoinAboutStoreNumberParameter.*
import static testdomain.CashBox.SendMoney.SendMoneyParameter.*
import static testdomain.CashBox.NotRefund.NotRefundParameter.*

import static spec.CashBoxSpec.*
import static spec.CashBoxSpec.LimitedValue.*
import static spec.CashBoxSpec.StoreNumber.*
import static spec.CashBoxSpec.StoreCoin.*
import static spec.RefundHappyPath.*
import static spec.Change.*

import static spec.implicit.ImplicitCashBox.*

@RunWith(Enclosed)
class CashBox extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().cashbox
    }

    static class SendBill extends ARRTSpec {

        String getFocusPoint() {
            "投入済み紙幣"
        }

        String getSpecName() {
            "紙幣だけを送出する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition | stimulation  | action    | invariant | combination
            Inserted     | NotifyRefund | Send      | None      | AllCombination
            NotInserted  | NotifyRefund | DoNothing | None      | AllCombination
        }

        @TupleConstructor
        static class SendBillParameter extends Parameter {
            def _name
            def isInsertedBill

            static SendBillParameter Inserted =
                [isInsertedBill: true, _name: "紙幣が入っている"]
            static SendBillParameter NotInserted =
                [isInsertedBill: false, _name: "紙幣が入っていない"]
        }

        AlgorithmBase algorithm = AllCombination

        static def NotifyRefund = [
                name: { "返金通知をする" },
                run: { Parameter parameter ->
                    given: "金庫を通知を送る機械とつないでおく"
                    if (parameter.isInsertedBill) {
                        and: "紙幣投入済データが" + parameter.isInsertedBill + "になっている"
                    }
                    if (parameter.amount && parameter.lessNumberOfCoin) {
                        and: "金庫内の硬貨を #parameter.amount を最少枚数で払い出すにはそれぞれ" + parameter.lessNumberOfCoin + "枚不足した枚数にしておく"
                    } else if (parameter.amount && parameter.storeCoin) {
                        and: "金庫内の硬貨をそれぞれ #parameter.storeCoin 枚にしておく"
                    } else if (parameter.storeAmount && parameter.storeNumber) {
                        and: "金庫内の貨幣をそれぞれ #parameter.storeNumber 枚にしておく"
                    } else {
                        and: "金庫内の貨幣を" + AllMax + "にしておく"
                    }
                }
        ] as Stimulation

        static def Send = [
                name: { "紙幣を送出する" },
                run: { stimulation, SendBillParameter preCondition ->
                    given: "硬貨は投入されていないことになっている"
                    when:
                    def result = stimulation()
                    then: "紙幣を送出する"
                    result
                }
        ] as Assertion

        static def DoNothing = [
                name: { Assertion.DoNothing },
                run: { stimulation, preCondition ->
                    when:
                    def result = stimulation()
                    then: "何も起こらない"
                    result
                }
        ] as Assertion
    }

    static class SendLeastNumberOfCoin extends ARRTSpec {
        String getSpecName() {
            "硬貨を最少枚数で送出する"
        }

        @Override
        String getFocusPoint() {
            "送出する枚数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition            | stimulation           | action          | invariant | combination
            _1CoinAmount            | SendBill.NotifyRefund | SendLeastOfCoin | None      | AllCombination
            _2CoinAmount            | SendBill.NotifyRefund | SendLeastOfCoin | None      | AllCombination
            _1AllCoinAmount         | SendBill.NotifyRefund | SendLeastOfCoin | None      | AllCombination
            InsertOnlyCoinMaxAmount | SendBill.NotifyRefund | SendLeastOfCoin | None      | AllCombination
            IntegerMaxPlus3Amount   | SendBill.NotifyRefund | SendLeastOfCoin | None      | AllCombination
            HappyPathAmount         | SendBill.NotifyRefund | SendLeastOfCoin | None      | AllCombination
        }

        @TupleConstructor
        static class SendLeastNumberOfCoinParameter extends Parameter {
            def _name
            def amount

            static SendLeastNumberOfCoinParameter _1CoinAmount =
                [amount: OneCoinAmount, _name: "硬貨1枚で送出できる金額"]
            static SendLeastNumberOfCoinParameter _2CoinAmount =
                [amount: TwoCoinAmount, _name: "硬貨2枚で送出できる金額"]
            static SendLeastNumberOfCoinParameter _1AllCoinAmount =
                [amount: AllOneCoinAmount, _name: "各硬貨1枚で送出できる金額"]
            static SendLeastNumberOfCoinParameter InsertOnlyCoinMaxAmount =
                [amount: InsertMaxOnlyCoinAmount, _name: "硬貨のみで投入できる上限金額(8200円)"]
            static SendLeastNumberOfCoinParameter IntegerMaxPlus3Amount =
                [amount: MaxAmount, _name: "実装言語のInteger最大値+3の(10円刻みに合わせた)金額"]
            static SendLeastNumberOfCoinParameter HappyPathAmount =
                [amount: HappyPaths, _name: "貨幣2枚以内で120円または150円の商品を買った場合の釣銭金額"]
        }

        AlgorithmBase algorithm = AllCombination

        static def SendLeastOfCoin = [
                name: { "硬貨を最少枚数で送出する" },
                run: { stimulation, SendLeastNumberOfCoinParameter parameter ->
                    given: "紙幣は投入されていないことになっている"
                    when:
                    def result = stimulation()
                    then: "parameter.amount円分の硬貨が送出される"
                    and: "硬貨が最小枚数である"
                    /**
                     * 次のような実装を想定
                     */
//                    def _500 = parameter.insert.intdiv(500)
//                    def _100 = (parameter.insert - (500 * _500)).intdiv(100)
//                    def _50 =  (parameter.insert - (500 * _500) - (100 * _100)).intdiv(50)
//                    def _10 =  (parameter.insert - (500 * _500) - (100 * _100) - (50 * _50)).intdiv(10)
//                    assert result._500 == _500
//                    assert result._100 == _100
//                    assert result._50 == _50
//                    assert result._10 == _10
                }

        ] as Assertion
    }

    static class SendLessNumberOfCoinAboutLessNumber extends ARRTSpec {
        String getSpecName() {
            "代替硬貨を用いてできるだけ少ない枚数で送出する"
        }

        @Override
        String getFocusPoint() {
            "最少枚数で返すには不足している硬貨"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition              | stimulation           | action                        | invariant | combination
            Amount500_LessOneType     | SendBill.NotifyRefund | SendLessOfCoinAboutLessNumber | None      | AllCombination
            Amount100_499_LessOneType | SendBill.NotifyRefund | SendLessOfCoinAboutLessNumber | None      | AllCombination
            Amount50_99_LessOneType   | SendBill.NotifyRefund | SendLessOfCoinAboutLessNumber | None      | AllCombination
            Amount500_LessTypes       | SendBill.NotifyRefund | SendLessOfCoinAboutLessNumber | None      | AllCombination
            Amount100_499_LessTypes   | SendBill.NotifyRefund | SendLessOfCoinAboutLessNumber | None      | AllCombination
            Amount50_99_LessTypes     | SendBill.NotifyRefund | SendLessOfCoinAboutLessNumber | None      | AllCombination
        }


        @TupleConstructor
        static class SendLessNumberOfCoinAboutLessNumberParameter extends Parameter {
            def _name
            def amount
            def lessNumberOfCoin
            static SendLessNumberOfCoinAboutLessNumberParameter Amount500_LessOneType =
                [amount: Amount500_, lessNumberOfCoin: [
                        [500: CashBoxSpec.rnd(1, StoreLimitCoin), 100: 0, 50: 0],
                        [500: CashBoxSpec.rnd(1, StoreLimitCoin), 100: CashBoxSpec.rnd(1, StoreLimitCoin), 50: 0],
                        [500: CashBoxSpec.rnd(1, StoreLimitCoin), 100: CashBoxSpec.rnd(1, StoreLimitCoin), 50: CashBoxSpec.rnd(1, StoreLimitCoin)]],
                        _name: "500円以上を最小枚数で返すにはいずれかの硬貨が任意枚足りない"]
            static SendLessNumberOfCoinAboutLessNumberParameter Amount100_499_LessOneType =
                [amount: Amount100_499, lessNumberOfCoin: [
                        [500: 0, 100: CashBoxSpec.rnd(1, StoreLimitCoin), 50: 0],
                        [500: 0, 100: CashBoxSpec.rnd(1, StoreLimitCoin), 50: CashBoxSpec.rnd(1, StoreLimitCoin)]],
                        _name: "500円未満100円以上を最小枚数で返すにはいずれかの硬貨が任意枚足りない"]
            static SendLessNumberOfCoinAboutLessNumberParameter Amount50_99_LessOneType =
                [amount: Amount50_99, lessNumberOfCoin: [[500: 0, 100: 0, 50: CashBoxSpec.rnd(1, StoreLimitCoin)]], _name: "100円未満50円以上を最小枚数で返すにはいずれかの硬貨が任意枚足りない"]
            static SendLessNumberOfCoinAboutLessNumberParameter Amount500_LessTypes =
                [amount: Amount500_, lessNumberOfCoin: [[500: 1, 100: 0, 50: 0], [500: 0, 100: 1, 50: 0], [500: 0, 100: 0, 50: 1]], _name: "500円以上を返すときに、10円といずれかの1枚不足している硬貨が1種類ある"]
            static SendLessNumberOfCoinAboutLessNumberParameter Amount100_499_LessTypes =
                [amount: Amount100_499, lessNumberOfCoin: [[500: 0, 100: 1, 50: 0], [500: 0, 100: 0, 50: 1]], _name: "500円未満100円以上を返すときに、10円と500円以外の1枚不足している硬貨が1種類ある。"]
            static SendLessNumberOfCoinAboutLessNumberParameter Amount50_99_LessTypes =
                [amount: Amount50_99, lessNumberOfCoin: [[500: 0, 100: 0, 50: 0]], _name: "100円未満50円以上を返すときに、10円と500円と100円以外の1枚不足している硬貨が1種類ある。"]
        }

        static def SendLessOfCoinAboutLessNumber = [
                name: { "硬貨を最小枚数にできるだけ近い枚数で送出する" },
                run: { stimulation, SendLessNumberOfCoinAboutLessNumberParameter parameter ->
                    given: "紙幣は投入されていないことになっている"
                    when:
                    def result = stimulation()
                    then: "parameter.amount円分の硬貨が送出される"
                    and: "硬貨を最小枚数にできるだけ近い枚数で送出する"
                    /**
                     * 次のような実装を想定
                     * _500 から _50はゼロ以上に切り上げる必要がある。
                     */
//                    def _500 = parameter.insert.intdiv(500) - parameter.lessNumberOfCoin[0]
//                    def _100 = (parameter.insert - (500 * _500)).intdiv(100) - parameter.lessNumberOfCoin[1]
//                    def _50 = (parameter.insert - (500 * _500) - (100 * _100)).intdiv(50) - parameter.lessNumberOfCoin[2]
//                    def _10 = (parameter.insert - (500 * _500) - (100 * _100) - (50 * _50)).intdiv(10)
//                    assert result._500 == _500
//                    assert result._100 == _100
//                    assert result._50 == _50
//                    assert result._10 == _10
                }
        ] as Assertion

        AlgorithmBase algorithm = AllCombination
    }

    static class SendLessNumberOfCoinAboutStoreNumber extends ARRTSpec {
        String getSpecName() {
            "代替硬貨を用いてできるだけ少ない枚数で送出する"
        }

        @Override
        String getFocusPoint() {
            "金庫内の硬貨の枚数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition                   | stimulation           | action                         | invariant | combination
            Replace500WithOneTypeOfCoin    | SendBill.NotifyRefund | SendLessOfCoinAboutStoreNumber | None      | AllCombination
            Replace500WithLastCoinAndOther | SendBill.NotifyRefund | SendLessOfCoinAboutStoreNumber | None      | AllCombination
            Replace100WithOneTypeOfCoin    | SendBill.NotifyRefund | SendLessOfCoinAboutStoreNumber | None      | AllCombination
            Replace100WithLastCoinAndOther | SendBill.NotifyRefund | SendLessOfCoinAboutStoreNumber | None      | AllCombination
            Replace50WithOneTypeOfCoin     | SendBill.NotifyRefund | SendLessOfCoinAboutStoreNumber | None      | AllCombination
            Replace50WithLastCoin          | SendBill.NotifyRefund | SendLessOfCoinAboutStoreNumber | None      | AllCombination
        }

        @TupleConstructor
        static class SendLessNumberOfCoinAboutStoreNumberParameter extends Parameter {
            def _name
            def amount
            def storeCoin

            static SendLessNumberOfCoinAboutStoreNumberParameter Replace500WithOneTypeOfCoin =
                [amount: OneCoin_500, storeCoin: Amount500_Replace1Type, _name: "500円を1種類の硬貨で代替する"]
            static SendLessNumberOfCoinAboutStoreNumberParameter Replace500WithLastCoinAndOther =
                [amount: OneCoin_500, storeCoin: Amount500_ReplaceSomeTypes_UseLastCoin, _name: "500円をいずれかの硬貨は必ず最後の1枚を用いて複数硬貨で代替する"]
            static SendLessNumberOfCoinAboutStoreNumberParameter Replace100WithOneTypeOfCoin =
                [amount: OneCoin_100, storeCoin: Amount100_Replace1Type, _name: "100円を1種類の硬貨で代替する"]
            static SendLessNumberOfCoinAboutStoreNumberParameter Replace100WithLastCoinAndOther =
                [amount: OneCoin_100, storeCoin: Amount100_ReplaceSomeTypes_UseLastCoin, _name: "100円をいずれかの硬貨は必ず最後の1枚を用いるように代替する"]
            static SendLessNumberOfCoinAboutStoreNumberParameter Replace50WithOneTypeOfCoin =
                [amount: OneCoin_50, storeCoin: Amount50_Replace1Type, _name: "50円を1種類の硬貨で代替する"]
            static SendLessNumberOfCoinAboutStoreNumberParameter Replace50WithLastCoin =
                [amount: OneCoin_50, storeCoin: Amount50_Replace1Type_UseLastCoin, _name: "50円をいずれかの硬貨は必ず最後の1枚を用いるように代替する"]
        }

        AlgorithmBase algorithm = AllCombination

        static def SendLessOfCoinAboutStoreNumber = [
                name: { "硬貨を最小枚数にできるだけ近い枚数で送出する" },
                run: { stimulation, SendLessNumberOfCoinAboutStoreNumberParameter parameter ->
                    given: "紙幣は投入されていないことになっている"
                    when:
                    def result = stimulation()
                    then: "parameter.amount円分の硬貨が送出される"
                    and: "硬貨を最小枚数にできるだけ近い枚数で送出する"
                    /**
                     * 次のような実装を想定
                     */
//                        def static div(insert, coin) {
//                            insert.intdiv(coin)
//                        }
//                    def div500 = div(parameter.insert, 500)
//                    def div100 = div(parameter.insert - (500 * _500), 100)
//                    def div50 = div(parameter.insert - (500 * _500) - (100 * _100), 50)
//                    def div10 = div(parameter.insert - (500 * _500) - (100 * _100) - (50 * _50), 10)
//                    def _500 = div500 <= storeCoin[500] ? div500 : storeCoin[500]
//                    def _100 = div100 <= storeCoin[100] ? div100 : storeCoin[100]
//                    def _50 = div50 <= storeCoin[50] ? div50 : storeCoin[50]
//                    def _10 =  div10 <= storeCoin[10] ? div10 : -1 //なんかエラー
//                    assert result._500 == _500
//                    assert result._100 == _100
//                    assert result._50 == _50
//                    assert result._10 == _10
                }
        ] as Assertion
    }

    static class NotRefund extends ARRTSpec {
        String getSpecName() {
            "返金通知が来ても何も起こらない"
        }

        @Override
        String getFocusPoint() {
            "残高と金庫内の各貨幣の枚数"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition                 | stimulation           | action             | invariant | combination
            NoAmount                     | SendBill.NotifyRefund | SendBill.DoNothing | None      | AllCombination
            StoreAmountLessThanAmount    | SendBill.NotifyRefund | SendBill.DoNothing | None      | AllCombination
            StoreNumberLessThanNecessary | SendBill.NotifyRefund | SendBill.DoNothing | None      | AllCombination
        }

        @TupleConstructor
        static class NotRefundParameter extends Parameter {
            def _name
            def amount
            def storeNumber

            static NotRefundParameter NoAmount =
                [amount: 0, storeNumber: AllRandom.target, _name: "残高が0"]
            static NotRefundParameter StoreAmountLessThanAmount =
                [amount: RndChoice, storeNumber: NoMoney, _name: "金庫内の貨幣が0"]
            static NotRefundParameter StoreNumberLessThanNecessary =
                [amount: AmountNecessary10, storeNumber: No10AndFullOthers, _name: "金庫内の貨幣の枚数で払える金額は残高より多いがぴったり払うには硬貨が足りない"]
    }

        AlgorithmBase algorithm = AllCombination
    }

    static class SendMoney extends ARRTSpec {
        String getSpecName() {
            "紙幣を入れた場合のみ紙幣を用いてなるべく少ない枚数で送出する"
        }

        @Override
        String getFocusPoint() {
            "残高と紙幣を投入したかどうか"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition                       | stimulation           | action         | invariant | combination
            OneBillAmount                      | SendBill.NotifyRefund | SendBillOrCoin | None      | AllCombination
            SendMoneyParameter.HappyPathAmount | SendBill.NotifyRefund | SendBillOrCoin | None      | AllCombination
            InsertMaxAmount                    | SendBill.NotifyRefund | SendBillOrCoin | None      | AllCombination
        }

        @TupleConstructor
        static class SendMoneyParameter extends Parameter {
            def _name
            def isInsertedBill
            def amount

            static SendMoneyParameter OneBillAmount =
                [isInsertedBill: [true, false], amount: OneBill_1000, _name: "紙幣1枚分の残高"]
            static SendMoneyParameter HappyPathAmount =
                [isInsertedBill: [true, false], amount: [Insert1020_Buy120.insert, Insert1050_Buy150.insert], _name: "紙幣と硬貨で購入する場合に投入しそうな残高"]
            static SendMoneyParameter InsertMaxAmount =
                [isInsertedBill: true, amount: InsertMax.amount(), _name: "紙幣と硬貨を投入枚数の上限まで入れた場合の残高"]
        }

        AlgorithmBase algorithm = AllCombination

        static def SendBillOrCoin = [
                name: { "貨幣を紙幣投入済みの場合のみ紙幣を用いて最少枚数で送出する" },
                run: { stimulation, SendMoneyParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "preCondition.amount円分の貨幣が送出される"
                    and: "紙幣投入済みの場合、紙幣と硬貨が送出される"
                    and: "紙幣投入済みでない場合、硬貨が送出される"

                    /**
                     * 次のような実装を想定
                     *
                     def static div(insert, coin) {insert.intdiv(coin)}def _1000 = parameter.isInsertedBill ? 1 : 0
                     def div500 = div(parameter.insert - (1000 * _1000), 500)
                     def div100 = div(parameter.insert - (1000 * _1000) - (500 * _500), 100)
                     def div50 = div(parameter.insert - (1000 * _1000) - (500 * _500) - (100 * _100), 50)
                     def div10 = div(parameter.insert - (1000 * _1000) - (500 * _500) - (100 * _100) - (50 * _50), 10)
                     def _500 = div500 <= storeNumber[_500] ? div500 : storeNumber[_500]
                     def _100 = div100 <= storeNumber[_100] ? div100 : storeNumber[_100]
                     def _50 = div50 <= storeNumber[_50] ? div50 : storeNumber[_50]
                     def _10 = div10 <= storeNumber[_10] ? div10 : -1 //なんかエラー
                     assert result._500 == _500
                     assert result._100 == _100
                     assert result._50 == _50
                     assert result._10 == _10
                     */
                    result
                }
        ] as Assertion
    }
}