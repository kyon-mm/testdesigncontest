package testdomain

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import groovy.transform.*
import spock.lang.*
import algorithm.*
import internals.*

import static algorithm.AlgorithmBase.*
import static internals.Invariant.*

import static internals.TestCaseFilter.*
import static testdomain.BillSlot.BillType.BillTypeParameter.*
import static testdomain.BillSlot.BillType.*
import static testdomain.AmountIndicator.DisplayAmount.DisplayAmountParameter.*
import static testdomain.AmountIndicator.DisplayAmount.*

@RunWith(Enclosed)
class BuyProduct extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().buyproduct
    }

    //TODO エラーを消すためにサンプルを書いてあります
    static class BuySpcifiedTemperature extends ARRTSpec {

        def setup() {
            algorithm = AllCombination
            debug = true
        }

        String getSpecName() {
            "商品を購入する"
        }

        @Override
        String getFocusPoint() {
            "商品の温度"
        }

        @Unroll
        def "#firstBehave : #firstCondition -> #secondBehave : #secondCondition"() {
            expect:
            integrate(firstBehave, new TestCaseFilter(* firstCondition), secondBehave, new TestCaseFilter(* secondCondition))
            where:
            firstBehave       | firstCondition    | secondBehave                  | secondCondition
            BillSlot.BillType | [AcceptBill, Any] | AmountIndicator.DisplayAmount | [_2ColumnAmount, NotifyAmount]
            BillSlot.BillType | [AcceptBill, Any] | AmountIndicator.DisplayAmount | [_2ColumnAmount, NotifyAmount]
        }
//
//        @Unroll
//        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
//            expect:
//            test(preCondition, stimulation, action, invariant)
//            where:
//            preCondition              | stimulation | action          | invariant                | combination
//            SAMPLE | sampleStimulation | sampleAction | sampleInvariant | AllCombination
//        }
//
//        @TupleConstructor
//        static class BuySpcifiedTemperatureParameter extends Parameter {
//            def _name
//            def factor1
//
//            static BuySpcifiedTemperatureParameter SAMPLE =
//                [factor1: [1,2,3], _name: "AAA"]
//        }
//
//        AlgorithmBase algorithm = AllCombination
//
//        static def sampleStimulation = [
//                stateName: { "テスト対象への操作、刺激" },
//                run: { BuySpcifiedTemperatureParameter parameter ->
//                    given: "事前準備をする"
//                }
//        ] as Stimulation
//
//        static def sampleAction = [
//                stateName: { "事後条件" },
//                run: { stimulation, BuySpcifiedTemperatureParameter preCondition ->
//                    when:
//                    def result = stimulation()
//                    then: "○○が〜〜でになっている"
//                    result
//                }
//        ] as Assertion
//
//        static def sampleInvariant = [
//                stateName: { "不変条件" },
//                run: { state ->
//                    assert state != null
//                }
//        ] as Invariant
//
    }
}