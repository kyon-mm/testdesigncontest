package testdomain

import algorithm.AlgorithmBase
import groovy.transform.TupleConstructor
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*
import spock.lang.Unroll

import static internals.Invariant.*
import static spec.Product.*
import static testdomain.ProductSlotCensor.NotifySendProduct.NotifySendProductParameter.*

@RunWith(Enclosed)
class ProductSlotCensor extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().productslotcensor
    }


    static class NotifySendProduct extends ARRTSpec {

        String getFocusPoint() {
            "商品種別"
        }

        String getSpecName() {
            "商品送出を通知する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition     | stimulation | action     | invariant | combination
            AllTypeOfProduct | SendProduct | DetectSend | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class NotifySendProductParameter extends Parameter {
            def _name
            def product

            static NotifySendProductParameter AllTypeOfProduct =
                [product: AllProduct, _name: "全ての商品"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def SendProduct = [
                name: { "商品を送出する" },
                run: { NotifySendProductParameter parameter ->
                    given: "検知結果を表示する画面と商品取り出し口センサをつないでおく" //たとえば画面とつないでテストできるとしたら
                    and: "画面に何も表示されていない"
                    and: "商品取り出し口を空にしておく"
                    when: "商品取り出し口に" + parameter.product + "を送出する"
                }
        ] as Stimulation

        static def DetectSend = [
                name: { "画面に通過と表示する" },
                run: { stimulation, NotifySendProductParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "画面に通過と表示されている"
                    result
                }
        ] as Assertion
    }
}