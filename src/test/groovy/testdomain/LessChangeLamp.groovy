package testdomain

import algorithm.AlgorithmBase
import groovy.transform.TupleConstructor
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import internals.*
import spock.lang.Unroll

import static internals.Invariant.*
import static spec.LessChangeLampSpec.*
import static testdomain.LessChangeLamp.LightLessChangeLamp.LightLessChangeLampParameter.*

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/10
 * Time: 14:30
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Enclosed)
class LessChangeLamp extends Target {
    @Override
    String getTargetName() {
        ubiquitous.dictionary().lesschangelamp
    }

    static class LightLessChangeLamp extends ARRTSpec {

        String getFocusPoint() {
            "釣銭切れ表示ランプの状態"
        }

        String getSpecName() {
            "点灯する"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition        | stimulation | action | invariant | combination
            LessChangeLampState | NotifyLight | Light  | None      | AlgorithmBase.AllCombination
        }

        @TupleConstructor
        static class LightLessChangeLampParameter extends Parameter {
            def _name
            def lampState

            static LightLessChangeLampParameter LessChangeLampState =
                [lampState: [Lighted, NotLighted], _name: "ランプの点灯状態"]
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def NotifyLight = [
                name: { "点灯通知をする" },
                run: { LightLessChangeLampParameter parameter ->
                    given: "通知する機器と釣銭切れ表示ランプをつないでおく"
                }
        ] as Stimulation

        static def Light = [
                name: { "釣銭切れ表示ランプを点灯する" },
                run: { stimulation, LightLessChangeLampParameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "釣銭切れ表示ランプが点灯している"
                    result
                }
        ] as Assertion
    }

    static class TurnOffLessChangeLamp extends ARRTSpec {
        String getSpecName() {
            "消灯する"
        }

        @Override
        String getFocusPoint() {
            "釣銭切れ表示ランプの状態"
        }

        @Unroll
        def "#preCondition -> #stimulation -> #action . 常に #invariant ."() {
            expect:
            test(preCondition, stimulation, action, invariant)
            where:
            preCondition        | stimulation   | action  | invariant | combination
            LessChangeLampState | NotifyTurnOff | TurnOff | None      | AlgorithmBase.AllCombination
        }

        AlgorithmBase algorithm = AlgorithmBase.AllCombination

        static def NotifyTurnOff = [
                name: { "消灯通知をする" },
                run: { Parameter parameter ->
                    given: "通知する機器と釣銭切れ表示ランプをつないでおく"
                }
        ] as Stimulation

        static def TurnOff = [
                name: { "釣銭切れ表示ランプを消灯する" },
                run: { stimulation, Parameter preCondition ->
                    when:
                    def result = stimulation()
                    then: "釣銭切れ表示ランプが消灯している"
                    result
                }
        ] as Assertion
    }
}
