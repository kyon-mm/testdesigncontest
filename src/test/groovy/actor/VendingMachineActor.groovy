package actor

import internals.Actor
import risk.FunctionalCompleteness
import internals.Risk
import internals.RiskBackLogItem

/**
 * Created with IntelliJ IDEA.
 * User: kyon_mm
 * Date: 2013/10/27
 * Time: 23:12
 * To change this template use File | Settings | File Templates.
 */
class VendingMachineActor {
    private static Actor customer = new Actor(
            name:"消費者",
            ubiquitous: new CustomerUbiquitous(),
            targetRisk: [FunctionalCompleteness]
    )
    private static Actor Owner = new Actor(name: "オーナー")
    private static Actor Supplier = new Actor(name: "飲料補充者")
    private static Actor Maintainer = new Actor(name: "自販機保守者")
    private static Actor Vendor = new Actor(name: "飲料物販売会社")

    static RiskBackLogItem Customer(Risk risk){
        customer.validate(risk)
        new RiskBackLogItem(actor: customer, risk: risk)
    }
}
