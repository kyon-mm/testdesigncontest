package actor

import groovy.transform.Canonical
import internals.Ubiquitous

/**
 * Created with IntelliJ IDEA.
 * User: kyon_mm
 * Date: 2013/10/27
 * Time: 10:51
 * To change this template use File | Settings | File Templates.
 */
@Canonical
class CustomerUbiquitous implements Ubiquitous {
    @Override
    Map dictionary() {
        [
                "tester": "テスター",
                "product": "商品",
                "amountIndicator": "金額表示機",
                "displayedAmount": "表示されている金額",
                "billSlot": "紙幣投入口",
                "cashBox": "金庫",
                "coinSlot": "硬貨投入口",
                "lessChangeLamp": "釣銭切れ表示ランプ",
                "lampState": "ランプの状態",
                "productSlotCensor": "商品取り出し口センサ",
                "rack": "ラック",
                "refundButton": "返金ボタン",
                "refundingLamp": "釣銭払出動作中表示ランプ",
                "roulette": "懸賞ルーレット",
                "saleButton": "販売ボタン",
                "timeout": "タイムアウト",
                "mode": "稼働モード",
                "money": "貨幣",
                "coin": "硬貨",
                "coinType": "硬貨の種類",
                "typeOfCoin": "硬貨の種類",
                "pushingTime": "押下時間",
                "isInsertedBill": "紙幣投入済",
                "insertedCoinNumber": "投入済硬貨枚数",
                "amount": "残高",
                "maxAmount": "投入可能最大残高",
                "change": "釣銭",
                "cashBoxState": "金庫の状態",
                "storeBill": "金庫内の紙幣の枚数",
                "storeCoin": "金庫内の硬貨の枚数",
                "storeNumber": "金庫内の枚数",
                "storeAmount": "金庫内の残高",
                "rackTemperature": "商品ラックの温度",
                "permitTemperatureMax": "許容商品温度最大値",
                "permitTemperatureMin": "許容商品温度最小値",
                "saleButtonState": "販売ボタンの状態",
                "saleButtonChangeNotice": "販売ボタン変更通知",
                "stockNumber": "ラック内商品数",
                "bill": "紙幣",
                "billType": "紙幣の種類",
                "diffWithInsertLimit": "投入上限枚数との差分",
                "lessNumberOfCoin": "硬貨の不足枚数",
                "winRate": "当たる確率",
                "time": "回数",
                "drawResult": "抽選結果",
                "turnTime": "抽選時間",
                "buyProduct":"商品を購入する"
        ].collectEntries { [it.key.toLowerCase(), it.value] }
    }
}
