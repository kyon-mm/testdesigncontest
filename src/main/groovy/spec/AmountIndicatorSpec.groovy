package spec

import internals.Equivalence
import internals.SpecBase
import static spec.Coin.*
import static spec.Bill.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/11
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */
class AmountIndicatorSpec extends SpecBase {
    int amount

    static Equivalence _1Column = [definition: { it.toString().size() == 1 }, target: [0, _1.value, _5.value, 9].collect { new AmountIndicatorSpec(amount: it) }]
    static Equivalence _2Column = [definition: { it.toString().size() == 2 }, target: [_10.value, _50.value, 99].collect { new AmountIndicatorSpec(amount: it) }]
    static Equivalence _3Column = [definition: { it.toString().size() == 3 }, target: [_100.value, _500.value, 999].collect { new AmountIndicatorSpec(amount: it) }]
    static Equivalence _4Column = [definition: { it.toString().size() == 4 }, target: [_1000.value, _2000.value, _5000.value, 9999].collect { new AmountIndicatorSpec(amount: it) }]

    static List<Equivalence> AllColumnRandom = [random(_1Column.target), random(_2Column.target), random(_3Column.target), random(_4Column.target)]

    @Override
    String toString() {
        amount
    }
}
