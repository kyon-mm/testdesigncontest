package spec

import internals.SpecBase

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/10
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
class Coin extends SpecBase{
    def value

    static Coin _1 = new Coin(value:1)
    static Coin _5 = new Coin(value:5)
    static Coin _10 = new Coin(value:10)
    static Coin _50 = new Coin(value:50)
    static Coin _100 = new Coin(value:100)
    static Coin _500 = new Coin(value:500)
    static Coin NonstandardCoin = new Coin(value:"ウォン")

    @Override
    String toString() {
        value instanceof Integer ? value + "円" : value + "(規格外)"
    }
}
