package spec

import internals.Equivalence
import internals.SpecBase

import static spec.implicit.ImplicitSaleButton.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/24
 * Time: 10:28
 * To change this template use File | Settings | File Templates.
 */
class RefundButtonSpec extends SpecBase {
    static Equivalence DetectableTime = [ definition: { it >= TimeToDetect }, target: [TimeToDetect, TimeToDetect + 0.01, 1, 10]]
    static Equivalence NotDetectableTime = [ definition: { it < TimeToDetect }, target: [TimeToDetect - 0.01, TimeToDetect - 0.02]]
}
