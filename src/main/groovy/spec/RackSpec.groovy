package spec

import internals.Equivalence
import internals.SpecBase

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/16
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
class RackSpec extends SpecBase {
    int no
    int stock
    def temperature
    def productPrice

    static RackSpec buildRack(int no) {
        new RackSpec(no: no)
    }

    static Equivalence AllRack = [definition: { it.class == RackSpec }, target: (1..30).collect { it -> buildRack(it) }]

    static Equivalence ExistStockNumber = [definition: {it.class == Integer && 0 <= it && it <= 50}, target : [1, 25, 49, 50]]
    static Equivalence NoStockNumber = [definition: {it.class == Integer && it == 0}, target : [0]]

    static int MaxPrice = 990
    static int MinPriceExceptZero = 10
    static int MinPrice = 0

    static class SuitableTemperature {
        def max
        def min
        def static Cold = new SuitableTemperature(max: 6, min: 1)
        def static Hot = new SuitableTemperature(max: 58, min: 52)
    }
}
