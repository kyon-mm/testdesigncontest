package spec

import internals.SpecBase

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/16
 * Time: 15:53
 * To change this template use File | Settings | File Templates.
 */
class Product extends SpecBase {
    String type

    static Product _250mlCan = new Product(type: "250ml缶")
    static Product _350mlCan = new Product(type: "350ml缶")
    static Product _500mlCan = new Product(type: "500ml缶")
    static Product _500mlPet = new Product(type: "500mlペットボトル")

    static List<Product> AllProduct = [_250mlCan, _350mlCan, _500mlCan, _500mlPet]

    @Override
    String toString() {
        type
    }
}
