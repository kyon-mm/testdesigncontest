package spec.implicit

import groovy.transform.Canonical
import internals.Equivalence
import internals.SpecBase

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/10
 * Time: 17:15
 * To change this template use File | Settings | File Templates.
 */
@Canonical
class ImplicitMaxAmountSpec extends SpecBase {
    BigInteger maxAmount

    static BigInteger MaxAmountDefault = 10000
    static BigInteger MaxAmountMin = 10
    static BigInteger MaxAmountMax = Integer.MAX_VALUE

    static Equivalence MaxAmounts = [
            definition:{it.class == ImplicitMaxAmountSpec && MaxAmountMin <= it.maxAmount && it.maxAmount <= MaxAmountMax},
            target: [MaxAmountMin, MaxAmountDefault, (MaxAmountMax + MaxAmountMin) / 2, MaxAmountMax].collect {new ImplicitMaxAmountSpec(maxAmount: it)}]

    @Override
    String toString(){
        maxAmount
    }
}
