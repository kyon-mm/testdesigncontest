package spec.implicit

import groovy.transform.Canonical
import internals.Equivalence

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/11/19
 * Time: 10:30
 * To change this template use File | Settings | File Templates.
 */
@Canonical
class ImplicitRoulette {
    BigDecimal turnTime
    BigDecimal winRate

    static BigDecimal TurnTimeDefault = 5
    static BigDecimal TurnTimeMax = 60
    static BigDecimal TurnTimeMin = 0.5

    static Equivalence TurnTimes = [
            definition: { it.turnTime.class == BigDecimal && TurnTimeMin <= it.turnTime },
            target: [TurnTimeMin, TurnTimeMin + 1, TurnTimeDefault, (TurnTimeMin + TurnTimeMax) / 2, TurnTimeMax - 1, TurnTimeMax, Float.MAX_VALUE].collect { new ImplicitRoulette(turnTime: it) }
    ]


    static BigDecimal ErrorRate = 0.01
    static BigDecimal AlwaysWinRate = 1
    static BigDecimal AlwaysLoseRate = 0
    static BigDecimal WinRateDefault = 0.05
    static BigDecimal WinRateMax = AlwaysWinRate
    static BigDecimal WinRateMin = AlwaysLoseRate

    static Equivalence AlwaysWin = [definition: { it.winRate == 1 }, target: [new ImplicitRoulette(winRate: AlwaysWinRate)]]
    static Equivalence AlwaysLose = [definition: { it.winRate == 0 }, target: [new ImplicitRoulette(winRate: AlwaysLoseRate)]]

    static Equivalence WinRates = [
            definition: { WinRateMin <= it.winRate && it.winRate <= WinRateMax },
            target: [WinRateMin, WinRateMin + 1, WinRateDefault, (WinRateMin + WinRateMax) / 2, WinRateMax - 1, WinRateMax].collect { new ImplicitRoulette(winRate: it) }]

    @Override
    String toString() {
        "[抽選時間:" + (turnTime != null ? turnTime : "未指定") + ", 当たる確率:" + (winRate != null ? winRate : "未指定") + "]"
    }
}