package spec

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/11/19
 * Time: 13:47
 * To change this template use File | Settings | File Templates.
 */
class RefundingLampSpec {
    boolean isFlashing

    static RefundingLampSpec Flashing = new RefundingLampSpec(isFlashing: true)
    static RefundingLampSpec NotFlashing = new RefundingLampSpec(isFlashing: false)

    @Override
    String toString(){
        isFlashing ? "点滅している" : "消灯している"
    }
}
