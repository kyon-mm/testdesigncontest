package spec

import groovy.transform.Canonical
import internals.Equivalence
import internals.IntRangeEx
import internals.SpecBase
import internals.ValueEx

import static internals.ValueEx.constants
import static spec.CashBoxSpec.StoreBill.*
import static spec.implicit.ImplicitCashBox.*
import static spec.CoinSlotSpec.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/24
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */
class CashBoxSpec extends SpecBase {
    static interface Amount {
        BigInteger amount()
    }

    def static rnd(n, m) {
        use(IntRangeEx) { m = (m == StoreLimitCoin) ? m - 1 : m; (n..m).random() }  //range内の要素個数上限がInteger.MAX_VALUEだったので0が入るとだめだった
    }

    //TODO Equivalence化したいけど難しいからおいておく
    static List<Amount> HappyPaths = constants(RefundHappyPath) { MetaProperty prop -> prop.type == RefundHappyPath }

    private static List<Amount> whole =
        constants(Change) { MetaProperty prop -> prop.type == Change } + HappyPaths + LimitedValue.Max

    static Equivalence Amount500_ = [definition: { 500 <= it.amount() }, target: whole.findAll { 500 <= it.amount() }]
    static Equivalence Amount100_499 = [definition: { 100 <= it.amount() && it.amount() <= 499 }, target: whole.findAll { 100 <= it.amount() && it.amount() <= 499 }]
    static Equivalence Amount50_99 = [definition: { 50 <= it.amount() && it.amount() <= 99 }, target: whole.findAll { 50 <= it.amount() && it.amount() <= 99 }]
    static Equivalence Amount0_49 = [definition: { 0 <= it.amount() && it.amount() <= 49 }, target: whole.findAll { 0 <= it.amount() && it.amount() <= 49 }]

    static Equivalence AmountNecessary10 = [definition: { it.amount() % 50 > 0 }, target: whole.findAll { it.amount() % 50 > 0 }]

    static Amount RndChoice = whole[new Random().nextInt(whole.size())]

    static abstract class LimitedValue extends ValueEx implements Amount {
        static LimitedValue Max = new LimitedValue() {
            @Override
            BigInteger amount() {
                Integer.MAX_VALUE + 3
            }
        }
        static Equivalence MaxAmount = [definition: { it.amount() > Integer.MAX_VALUE }, target: [Max]]

        static LimitedValue Min = new LimitedValue() {
            @Override
            BigInteger amount() {
                Integer.MIN_VALUE
            }
        }

        @Override
        String toString() {
            this.amount() + "円"
        }
    }

    @Canonical
    static class StoreNumber implements Amount {
        StoreBill storeBill
        StoreCoin storeCoin

        static StoreNumber AllMax = new StoreNumber(storeBill: new StoreBill(_1000: StoreLimit1000), storeCoin: new StoreCoin(_10: StoreLimitCoin, _50: StoreLimitCoin, _100: StoreLimitCoin, _500: StoreLimitCoin))

        static Equivalence NoMoney = [definition: { it.amount() == 0 }, target: [new StoreNumber(storeBill: new StoreBill(_1000: 0), storeCoin: new StoreCoin(_10: 0, _50: 0, _100: 0, _500: 0))]]
        static Equivalence AllRandom = [definition: {
            0 <= it.storeCoin._10 && it.storeCoin._10 <= StoreLimitCoin &&
                    0 <= it.storeCoin._50 && it.storeCoin._50 <= StoreLimitCoin &&
                    0 <= it.storeCoin._100 && it.storeCoin._100 <= StoreLimitCoin &&
                    0 <= it.storeCoin._500 && it.storeCoin._500 <= StoreLimitCoin &&
                    0 <= it.storeBill._1000 && it.storeBill._1000 <= StoreLimit1000
        },
                target: [new StoreNumber(storeBill: new StoreBill(_1000: CashBoxSpec.rnd(0, 1000)), storeCoin: new StoreCoin(_10: CashBoxSpec.rnd(0, StoreLimitCoin), _50: CashBoxSpec.rnd(0, StoreLimitCoin), _100: CashBoxSpec.rnd(0, StoreLimitCoin), _500: CashBoxSpec.rnd(0, StoreLimitCoin)))]]
        static Equivalence No10AndFullOthers = [
                definition: { it.storeCoin._10 == 0 && it.storeCoin._50 == StoreLimitCoin && it.storeCoin._100 == StoreLimitCoin && it.storeCoin._500 == StoreLimitCoin && it.storeBill._1000 == StoreLimit1000 },
                target: [new StoreNumber(storeBill: new StoreBill(_1000: StoreLimit1000), storeCoin: new StoreCoin(_10: 0, _50: StoreLimitCoin, _100: StoreLimitCoin, _500: StoreLimitCoin))]]

        BigInteger amount() {
            (this.storeBill.amount() + this.storeCoin.amount()).toBigInteger()
        }

        @Override
        String toString() {
            storeCoin.toString() + ", " + storeBill.toString()
        }
    }

    @Canonical
    static class StoreBill implements Amount {
        int _1000

        static def StoreLimit1000 = 1000

        @Override
        public String toString() {
            "1000円:" + _1000.toString() + "枚"
        }

        @Override
        BigInteger amount() {
            _1000 * 1000
        }
    }

    @Canonical
    static class StoreCoin implements Amount {
        BigInteger _10
        BigInteger _50
        BigInteger _100
        BigInteger _500

        static Equivalence Amount500_Replace1Type = [
                definition: { it.amount() >= Coin._500.value && it._500 == 0 && (it._100 >= 5 || it._100 == 0 && it._50 >= 10 || it._100 == 0 && it._50 >= 0 && it._10 >= 50) },
                target: [
                        new StoreCoin(_10: CashBoxSpec.rnd(0, StoreLimitCoin), _50: CashBoxSpec.rnd(0, StoreLimitCoin), _100: CashBoxSpec.rnd(5, StoreLimitCoin), _500: 0),
                        new StoreCoin(_10: CashBoxSpec.rnd(0, StoreLimitCoin), _50: CashBoxSpec.rnd(10, StoreLimitCoin), _100: 0, _500: 0),
                        new StoreCoin(_10: CashBoxSpec.rnd(50, StoreLimitCoin), _50: 0, _100: 0, _500: 0)
                ]
        ]
        static Equivalence Amount500_ReplaceSomeTypes_UseLastCoin = [
                definition: { it.amount() >= Coin._500.value && it._500 == 0 && it._100 < 5 && (it._100 > 0 || it._100 == 0 && 0 < it._50 && it._50 < 10) },
                target: [
                        new StoreCoin(_10: CashBoxSpec.rnd(0, StoreLimitCoin), _50: CashBoxSpec.rnd(8, StoreLimitCoin), _100: 1, _500: 0),
                        new StoreCoin(_10: CashBoxSpec.rnd(35, StoreLimitCoin), _50: 1, _100: 1, _500: 0),
                        new StoreCoin(_10: 45, _50: 1, _100: 0, _500: 0)
                ]
        ]
        static Equivalence Amount100_Replace1Type = [
                definition: { it.amount() >= Coin._100.value && it._100 == 0 && (it._50 >= 2 || it._50 == 0 && it._10 >= 10) },
                target: [
                        new StoreCoin(_10: CashBoxSpec.rnd(0, StoreLimitCoin), _50: CashBoxSpec.rnd(2, StoreLimitCoin), _100: 0, _500: CashBoxSpec.rnd(0, StoreLimitCoin)),
                        new StoreCoin(_10: CashBoxSpec.rnd(10, StoreLimitCoin), _50: CashBoxSpec.rnd(2, StoreLimitCoin), _100: 0, _500: CashBoxSpec.rnd(0, StoreLimitCoin)),
                ]
        ]
        static Equivalence Amount100_ReplaceSomeTypes_UseLastCoin = [
                definition: { it.amount() >= Coin._100.value && it._100 == 0 && it._50 == 1 && it._10 == 5 },
                target: [
                        new StoreCoin(_10: 5, _50: 1, _100: 0, _500: CashBoxSpec.rnd(0, StoreLimitCoin))
                ]
        ]
        static Equivalence Amount50_Replace1Type = [
                definition: { it.amount() >= Coin._50.value && it._50 == 0 && it._10 >= 5 },
                target: [
                        new StoreCoin(_10: CashBoxSpec.rnd(5, StoreLimitCoin), _50: 0, _100: CashBoxSpec.rnd(0, StoreLimitCoin), _500: CashBoxSpec.rnd(0, StoreLimitCoin))
                ]
        ]
        static Equivalence Amount50_Replace1Type_UseLastCoin = [
                definition: { it.amount() >= Coin._50.value && it._50 == 0 && it._10 == 5 },
                target: [
                        new StoreCoin(_10: 5, _50: 0, _100: CashBoxSpec.rnd(0, StoreLimitCoin), _500: CashBoxSpec.rnd(0, StoreLimitCoin))
                ]
        ]

        @Override
        public String toString() {
            "10円:" + _10.toString() + "枚, 50円:" + _50.toString() + "枚, 100円:" + _100.toString() + "枚, 500円:" + _500.toString() + "枚"
        }

        @Override
        BigInteger amount() {
            _10 * 10 + _50 * 50 + _100 * 100 + _500 * 500
        }
    }
}

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/11/12
 * Time: 17:37
 * To change this template use File | Settings | File Templates.
 */
@Canonical
class RefundHappyPath extends ValueEx implements CashBoxSpec.Amount {
    int insert
    int price

    BigInteger amount() {
        insert - price
    }

//TODO Equivalence化したいけど難しいからおいておく
    static RefundHappyPath Insert30_Buy120 = new RefundHappyPath(insert: 150, price: 120)
    static RefundHappyPath Insert200_Buy150 = new RefundHappyPath(insert: 200, price: 150)
    static RefundHappyPath Insert200_Buy120 = new RefundHappyPath(insert: 200, price: 120)
    static RefundHappyPath Insert500_Buy150 = new RefundHappyPath(insert: 500, price: 150)
    static RefundHappyPath Insert500_Buy120 = new RefundHappyPath(insert: 500, price: 120)
    static RefundHappyPath Insert1000_Buy150 = new RefundHappyPath(insert: 1000, price: 150)
    static RefundHappyPath Insert1000_Buy120 = new RefundHappyPath(insert: 1000, price: 120)

    static RefundHappyPath Insert1050_Buy120 = new RefundHappyPath(insert: 1050, price: 120)
    static RefundHappyPath Insert1050_Buy150 = new RefundHappyPath(insert: 1050, price: 150)
    static RefundHappyPath Insert1020_Buy120 = new RefundHappyPath(insert: 1020, price: 120)

    @Override
    String toString() {
        this.amount() + "円(" + insert + "円投入して" + price + "円の商品を買った釣銭)"
    }
}

@Canonical
class Change extends ValueEx implements CashBoxSpec.Amount {
    int _10
    int _50
    int _100
    int _500
    int _1000

    BigInteger amount() {
        _10 * 10 + _50 * 50 + _100 * 100 + _500 * 500 + _1000 * 1000
    }

    BigInteger number() {
        _10 + _50 + _100 + _500 + _1000
    }

    static Equivalence OneCoin_10 = [definition: { it.amount() == Coin._10.value && it.number() == 1 }, target: [new Change(_10: 1)]]
    static Equivalence OneCoin_50 = [definition: { it.amount() == Coin._50.value && it.number() == 1 }, target: [new Change(_50: 1)]]
    static Equivalence OneCoin_100 = [definition: { it.amount() == Coin._100.value && it.number() == 1 }, target: [new Change(_100: 1)]]
    static Equivalence OneCoin_500 = [definition: { it.amount() == Coin._500.value && it.number() == 1 }, target: [new Change(_500: 1)]]
    static Equivalence OneBill_1000 = [definition: { it.amount() == Bill._1000.value && it.number() == 1 }, target: [new Change(_1000: 1)]]

    static Change TwoCoin_60 = new Change(_10: 1, _50: 1)
    static Change TwoCoin_110 = new Change(_10: 1, _100: 1)
    static Change TwoCoin_150 = new Change(_50: 1, _100: 1)
    static Change TwoCoin_510 = new Change(_10: 1, _500: 1)
    static Change TwoCoin_550 = new Change(_50: 1, _500: 1)
    static Change TwoCoin_600 = new Change(_100: 1, _500: 1)

    static Change AllOneCoin = new Change(_10: 1, _50: 1, _100: 1, _500: 1)
    static Change InsertMaxOnlyCoin = new Change(_10: insertLimit(Coin._10), _50: insertLimit(Coin._50), _100: insertLimit(Coin._100), _500: insertLimit(Coin._500))
    static Change InsertMax = new Change(_10: insertLimit(Coin._10), _50: insertLimit(Coin._50), _100: insertLimit(Coin._100), _500: insertLimit(Coin._500), _1000: BillSlotSpec.insertLimit(Bill._1000))

    static List<Equivalence> OneCoinAmount = [OneCoin_10, OneCoin_50, OneCoin_100, OneCoin_500]
    static Equivalence TwoCoinAmount = [definition: { it.number() == 2 }, target: [TwoCoin_60, TwoCoin_110, TwoCoin_150, TwoCoin_510, TwoCoin_550, TwoCoin_600]]
    static Equivalence AllOneCoinAmount = [definition: { it.number() == NormalCoinTypes.target.size() }, target: [AllOneCoin]]
    static Equivalence InsertMaxOnlyCoinAmount = [definition: { it.number() == NormalCoinTypes.target.sum { insertLimit(it) } }, target: [InsertMaxOnlyCoin]]

    @Override
    String toString() {
        this.amount() + "円"
    }
}
