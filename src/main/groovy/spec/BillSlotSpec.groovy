package spec

import internals.Equivalence
import internals.SpecBase

import static spec.Bill.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/10
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
class BillSlotSpec extends SpecBase {
    static Equivalence NormalBillTypes = [definition: { new Bill().values().containsValue(it) && it == _1000 }, target: [_1000]]
    static Equivalence ErrorBillTypes = [definition: { new Bill().values().containsValue(it) && it != _1000 }, target: [_2000, _5000, _10000, NonstandardBill]]

    static def insertLimit = { Bill bill ->
        switch (bill) {
            case _1000:
                1
                break
            default:
                throw new Exception()
                break
        }
    }
}
