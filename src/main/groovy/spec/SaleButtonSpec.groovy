package spec

import groovy.transform.Canonical
import internals.Equivalence
import internals.SpecBase
import internals.ValueEx

import static spec.implicit.ImplicitSaleButton.*

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/10/29
 * Time: 19:58
 * To change this template use File | Settings | File Templates.
 */
class SaleButtonSpec extends SpecBase {
    int no

    static SaleButtonSpec buildButton(int no) {
        new SaleButtonSpec(no: no)
    }

    static List<SaleButtonSpec> AllButton = (1..30).collect { it -> buildButton(it) }

    static Equivalence DetectableTime = [definition: { it >= TimeToDetect }, target: [TimeToDetect, TimeToDetect + 0.01, 1, 10]]
    static Equivalence NotDetectableTime = [definition: { it < TimeToDetect }, target: [TimeToDetect - 0.01, TimeToDetect - 0.02]]

    @Override
    String toString(){
        no
    }

    @Canonical
    static class SaleButtonState extends ValueEx {
        def state
        def static NotLight = new SaleButtonState(state: "すべてのランプが点灯していない")
        def static LightSaleButton = new SaleButtonState(state: "販売ボタンが点灯している")
        def static FlashSaleButton = new SaleButtonState(state: "販売ボタンが点滅している")
        def static LightPreparingLamp = new SaleButtonState(state: "準備中ランプが点灯している")
        def static LightSoldoutLamp = new SaleButtonState(state: "売り切れランプが点灯している")

        static Equivalence AllState = [definition: { it.class == SaleButtonState }, target: new SaleButtonState().values()]
        static Equivalence PushableState = [definition: {it.class == SaleButtonState && it == LightSaleButton}, target : [LightSaleButton]]
        static Equivalence NotPushableState = [definition: {it.class == SaleButtonState && it != LightSaleButton}, target: AllState.target.minus(PushableState.target)]

        @Override
        String toString() {
            state
        }
    }
}
