package spec

import groovy.transform.Canonical
import internals.Equivalence
import internals.SpecBase

import static spec.Coin.*
import static spec.CoinSlotSpec.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/10
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
class CoinSlotSpec extends SpecBase {
    static Equivalence NormalCoinTypes = [definition: { new Coin().values().contains(it) && ([_10, _50, _100, _500].contains(it)) }, target: [_10, _50, _100, _500]]
    static Equivalence ErrorCoinTypes = [definition: { new Coin().values().contains(it) && ([_10, _50, _100, _500].contains(it) == false) }, target: [_1, _5, NonstandardCoin]]

    static def insertLimit = { Coin coin ->
        switch (coin) {
            case [_10, _50, _100]:
                20
                break
            case _500:
                10
                break
            default:
                throw new Exception()
                break
        }
    }

    @Canonical
    static class InsertedCoinNumber {
        int _10
        int _50
        int _100
        int _500

        static Equivalence InsertableAll = [
                definition: { it._10 < insertLimit(Coin._10) && it._50 < insertLimit(Coin._50) && it._100 < insertLimit(Coin._100) && it._500 < insertLimit(Coin._500) },
                target: [new InsertedCoinNumber(_10: insertLimit(Coin._10) - 1, _50: insertLimit(Coin._50) - 1,_100:insertLimit(Coin._100) - 1, _500:insertLimit(Coin._500) - 1)]]
        static Equivalence MaxAll = [
                definition: { it._10 == insertLimit(Coin._10) && it._50 == insertLimit(Coin._50) && it._100 == insertLimit(Coin._100) && it._500 == insertLimit(Coin._500) },
                target: [new InsertedCoinNumber(_10: insertLimit(Coin._10), _50: insertLimit(Coin._50),_100:insertLimit(Coin._100), _500:insertLimit(Coin._500))]]
        //TODO 以下4つは他の因子の値を使って水準を決定できるようになったら1つにまとめたい
        static Equivalence MaxExclude_10 = [
                definition: { it._10 == insertLimit(Coin._10) - 1 && it._50 == insertLimit(Coin._50) && it._100 == insertLimit(Coin._100) && it._500 == insertLimit(Coin._500) },
                target: [new InsertedCoinNumber(_10: insertLimit(Coin._10) - 1, _50: insertLimit(Coin._50),_100:insertLimit(Coin._100), _500:insertLimit(Coin._500))]]
        static Equivalence MaxExclude_50 = [
                definition: { it._10 == insertLimit(Coin._10) && it._50 == insertLimit(Coin._50) - 1 && it._100 == insertLimit(Coin._100) && it._500 == insertLimit(Coin._500) },
                target: [new InsertedCoinNumber(_10: insertLimit(Coin._10), _50: insertLimit(Coin._50) - 1,_100:insertLimit(Coin._100), _500:insertLimit(Coin._500))]]
        static Equivalence MaxExclude_100 = [
                definition: { it._10 == insertLimit(Coin._10) && it._50 == insertLimit(Coin._50) && it._100 == insertLimit(Coin._100) - 1 && it._500 == insertLimit(Coin._500) },
                target: [new InsertedCoinNumber(_10: insertLimit(Coin._10), _50: insertLimit(Coin._50),_100:insertLimit(Coin._100) - 1, _500:insertLimit(Coin._500))]]
        static Equivalence MaxExclude_500 = [
                definition: { it._10 == insertLimit(Coin._10)&& it._50 == insertLimit(Coin._50) && it._100 == insertLimit(Coin._100) && it._500 == insertLimit(Coin._500) - 1  },
                target: [new InsertedCoinNumber(_10: insertLimit(Coin._10), _50: insertLimit(Coin._50),_100:insertLimit(Coin._100), _500:insertLimit(Coin._500) - 1 )]]

        @Override
        String toString() {
            "10:" + _10 + ", 50:" +_50 + ", 100:" + _100 + ", 500:" + _500
        }
    }
}
