package spec

import internals.SpecBase

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/10
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
class Bill extends SpecBase{
    def value

    static Bill _1000 = new Bill(value: 1000)
    static Bill _2000 = new Bill(value: 2000)
    static Bill _5000 = new Bill(value: 5000)
    static Bill _10000 = new Bill(value: 10000)
    static Bill NonstandardBill = new Bill(value: "ウォン")

    @Override
    String toString() {
        value instanceof Integer ? value + "円" : value + "(規格外)"
    }
}
