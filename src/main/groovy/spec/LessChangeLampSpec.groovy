package spec

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/11/19
 * Time: 13:15
 * To change this template use File | Settings | File Templates.
 */
class LessChangeLampSpec {
    boolean isLighted

    static LessChangeLampSpec Lighted = new LessChangeLampSpec(isLighted: true)
    static LessChangeLampSpec NotLighted = new LessChangeLampSpec(isLighted: false)

    @Override
    String toString(){
        isLighted ? "点灯している" : "消灯している"
    }
}
