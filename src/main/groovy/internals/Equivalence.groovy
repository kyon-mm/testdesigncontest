package internals

import groovy.transform.Immutable
import groovy.transform.TupleConstructor

@Immutable
@TupleConstructor
class Equivalence<T> {
    Map level(Map result){
        result.findAll {definition.call(it)}
    }
    boolean validate(){
        target.each {assert definition.call(it)}
    }
    /**
     * 同値分割の定義となる式
     * 一つのオブジェクトを受け取る
     */
    Closure definition
    List<T> target
}
