package internals

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/10/27
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
class IntRangeEx {
    static random(IntRange self, args){
        int from = self.isReverse() ? self.to : self.from
        int to = self.isReverse() ? from : self.to
        int size = to - from + 1
        (Math.floor(Math.random() * size) + from) as int
    }
}
