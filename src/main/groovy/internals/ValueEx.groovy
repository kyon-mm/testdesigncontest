package internals

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/11/12
 * Time: 10:57
 * To change this template use File | Settings | File Templates.
 */
class ValueEx {
    List<Objects> values() {
        getValidProperties().collect { it.getProperty(this) }
    }

    def randomValue() {
        def vs = values()
        vs[new Random().nextInt(vs.size())]
    }

    List values(Closure cls) {
        getValidProperties().findAll { cls.call(it) }.collect { it.getProperty(this) }
    }

    static List constants(Class clazz, Closure cls) {
        getValidConstants(clazz).findAll { cls.call(it) }.collect{ it.getProperty(this) }
    }

    private ArrayList<MetaProperty> getValidProperties() {
        this.metaClass.properties.findAll { it.getProperty(this) != null && it.name.contains("class") == false}
    }

    private static ArrayList<MetaProperty> getValidConstants(Class clazz) {
        clazz.metaClass.properties.findAll()
    }
}
