package algorithm

/**
 * Created with IntelliJ IDEA.
 * User: kyon-mm
 * Date: 13/10/10
 * Time: 14:21
 * To change this template use File | Settings | File Templates.
 */
interface AlgorithmBase {
    public List gen(parameter);
    String getName()
    public static AllCombination = new AlgorithmBase() {

        public List gen(parameter){
            def props =  parameter.metaClass.properties.findAll{!["class", "_name"].contains(it.name) && it.type != parameter.class }.sort {it.name}
            def values = props*.getProperty(parameter)
            if(values.any{it instanceof Collection}){
                return values.combinations().collect {
                    parameter.class.newInstance(parameter._name, *it)
                }
            }
            else{
                return [parameter]
            }
        }

        @Override
        String getName() {
            "全組み合わせ"
        }

        private static ArrayList<MetaProperty> getValidProperties(parameter) {
            parameter.metaClass.properties.findAll{ it.name != "class" && it.getProperty(parameter) != null }
        }
    }
}
